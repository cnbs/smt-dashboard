using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace smt_dashboard.Middleware
{
    public sealed class CspOptions
    {
        public List<string> Defaults { get; set; } = new List<string>();
        public List<string> Scripts { get; set; } = new List<string>();
        public List<string> Styles { get; set; } = new List<string>();
        public List<string> Images { get; set; } = new List<string>();
        public List<string> Fonts { get; set; } = new List<string>();
        public List<string> Media { get; set; } = new List<string>();

        public List<string> Connect { get; set; } = new List<string>();
    }

    public sealed class CspOptionsBuilder
    {
        private readonly CspOptions options = new CspOptions();

        internal CspOptionsBuilder()
        {
        }

        public CspDirectiveBuilder Defaults { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Scripts { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Styles { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Images { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Fonts { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Media { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Connect { get; set; } = new CspDirectiveBuilder();

        internal CspOptions Build()
        {
            this.options.Defaults = this.Defaults.Sources;
            this.options.Scripts = this.Scripts.Sources;
            this.options.Styles = this.Styles.Sources;
            this.options.Images = this.Images.Sources;
            this.options.Fonts = this.Fonts.Sources;
            this.options.Media = this.Media.Sources;
            this.options.Connect = this.Connect.Sources;
            return this.options;
        }
    }

    public sealed class CspDirectiveBuilder
    {
        internal CspDirectiveBuilder()
        {
        }

        internal List<string> Sources { get; set; } = new List<string>();

        public CspDirectiveBuilder AllowSelf() => Allow("'self'");
        public CspDirectiveBuilder AllowNone() => Allow("none");
        public CspDirectiveBuilder AllowAny() => Allow("*");

        public CspDirectiveBuilder Allow(string source)
        {
            this.Sources.Add(source);
            return this;
        }

    }

    public sealed class CspMiddleware
    {
        private const string HEADER = "Content-Security-Policy";
        private readonly RequestDelegate next;
        private readonly CspOptions options;

        public CspMiddleware(
            RequestDelegate next, CspOptions options)
        {
            this.next = next;
            this.options = options;
        }

        public async Task Invoke(HttpContext context)
        {
            context.Response.Headers.Add(HEADER, GetHeaderValue(context));
            await this.next(context);
        }

        private string GetHeaderValue(HttpContext context)
        {
            byte[] nonceBytes = new byte[32];
            using(var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(nonceBytes);
            }

            var nonce = Convert.ToBase64String(nonceBytes);
            context.Items["NONCE"] = nonce;
            
            var value = "";
            value += GetDirective("default-src", this.options.Defaults);
            value += GetDirective("script-src", this.options.Scripts, nonce);
            value += GetDirective("style-src", this.options.Styles, nonce);
            value += GetDirective("img-src", this.options.Images);
            value += GetDirective("font-src", this.options.Fonts);
            value += GetDirective("media-src", this.options.Media);
            value += GetDirective("connect-src", this.options.Connect);
            return value;
        }

        private string GetDirective(string directive, List<string> sources, string nonce = "")
        {
            if (nonce != "")
            {
                sources = new List<string>(sources);
                sources.Add("'nonce-" + nonce + "'");
            }
            return sources.Count > 0 ? $"{directive} {string.Join(" ", sources)}; " : "";
        }
    }

    public static class CspMiddlewareExtensions
    {
        public static IApplicationBuilder UseCsp(
            this IApplicationBuilder app, Action<CspOptionsBuilder> builder)
        {
            var newBuilder = new CspOptionsBuilder();
            builder(newBuilder);

            var options = newBuilder.Build();
            return app.UseMiddleware<CspMiddleware>(options);
        }
    }
}