﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using smt_dashboard.Models;
using smt_dashboard.ViewModels;

namespace smt_dashboard.Controllers
{
    [Route("createfmorder")]
    [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
    public class FMOrderController : Controller
    {
        private IConfiguration _config;

        public FMOrderController(IConfiguration config)
        {
            _config = config;
        }

        [Route("{*page}")]
        public IActionResult Index()
        {
            ViewData["NONCE"] = HttpContext.Items["NONCE"];
            DashboardViewModel model = new DashboardViewModel
            {
                SAPServer = _config["APIConfig:BaseUrl"],
                AppId = _config["APIConfig:apiid"],
                BuildNumber = typeof(DashboardController).Assembly.GetName().Version.ToString(),
                FMOrderUrl = _config["FMOrderAPIConfig:BaseUrl"],
                Base = _config["UrlBase"]
            };
            return View(model);
        }

        [HttpGet("/api/fmorder/check-login")]
        public async Task<IActionResult> CheckLogin([FromServices] IFmOrderService fmOrderService)
        {
            var statusTask = fmOrderService.CheckLogin();
        
            var status = await statusTask;

            //status.IsSuccess = true;

            return base.Ok(status);
        }

        [HttpGet("/api/fmorder/repairtypes")]
        public async Task<IActionResult> GetRepairTypes([FromServices] IFmOrderService fmOrderService, string equipmentNumber)
        {
            var repairTypesTask = fmOrderService.GetRepairTypes(equipmentNumber);

            //var repairTypesTask = fmOrderService.GetMockRepairTypes(equipmentNumber);
            
            var repairTypes = await repairTypesTask;

            return base.Ok(repairTypes);
        }

        [HttpGet("/api/fmorder/technicians")]
        public async Task<IActionResult> GetTechnicians([FromServices] IFmOrderService fmOrderService, string officeId)
        {
            var techniciansTask = fmOrderService.GetTechnicians(officeId);

            //var techniciansTask = fmOrderService.GetMockTechnicians(officeId);

            var technicians = await techniciansTask;

            return base.Ok(technicians);
        }

        [HttpGet("/api/fmorder/details-lists")]
        public async Task<IActionResult> GetDetailsLists([FromServices] IFmOrderService fmOrderService,string equipmentNumber, string officeId)
        {
            //var techniciansTask = fmOrderService.GetTechnicians(officeId);

            var techniciansTask = fmOrderService.GetMockTechnicians(officeId);

            var technicians = await techniciansTask;

            //var repairTypesTask = fmOrderService.GetRepairTypes(equipmentNumber);

            var repairTypesTask = fmOrderService.GetMockRepairTypes(equipmentNumber);

            var repairTypes = await repairTypesTask;

            var lists = new
            {
                Technicians = technicians,
                RepairTypes = repairTypes
            };

            return base.Ok(lists);
        }

        [HttpPost("/api/create-fm-order")]
        public async Task<IActionResult> CreateFMOrder([FromServices] IFmOrderService fmOrderService, [FromBody] FmOrder order)
        {
            var response = await fmOrderService.CreateFMOrder(order);

            return base.Ok(response);
        }

        [HttpPost("/api/update-fm-order")]
        public async Task<IActionResult> UpdateFMOrder([FromServices] IFmOrderService fmOrderService, [FromBody] FmOrder order)
        {
            await fmOrderService.UpdateFmOrder(order);

            return base.Ok();
        }

        [HttpGet("/api/fmorder/building/{buildingNumber}/equipment")]
        public async Task<IActionResult> GetEquipment([FromServices] IFmOrderService fmOrderService, string buildingNumber)
        {
            IEnumerable<Equipment> equipment = await fmOrderService.GetEquipment(buildingNumber);

            return base.Ok(equipment);
        }

        [HttpGet("/api/fmorder/building-search")]
        public async Task<IActionResult> SearchBuilding([FromServices] IFmOrderService fmOrderService, [FromQuery] string name, [FromQuery] string searchType)
        {
            IEnumerable<Building> buildings = await fmOrderService.SearchBuilding(name, searchType);

            return base.Ok(buildings);
        }

        [HttpGet("/api/fmorders")]
        public async Task<IActionResult> SearchFMorders( [FromServices] IFmOrderService fmOrderService, [FromQuery] string equipment, [FromQuery] string officeId)
        {
            IEnumerable<FmOrder> fmOrders = await fmOrderService.SearchFmOrders(equipment, officeId);

            return base.Ok(fmOrders);
        }

    }
}
