using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using smt_dashboard.Interfaces;
using smt_dashboard.Models;

namespace smt_dashboard.Controllers
{
    [ResponseCache(NoStore =true, Location =ResponseCacheLocation.None)]
    public class SAPController : Controller
    {
        private readonly IAuthorizationHeaderService _authHeaderService;
        private AuthenticatedUser _user;

        public SAPController(IAuthorizationHeaderService service)
        {
            _authHeaderService = service;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string authHeader = Request.Headers["Authorization"];
            if (authHeader == null) return;
            string supervisorIdListString = Request.Headers["Supers"];

            _user = _authHeaderService.GetAuthorizedUserFromHeaderAuthentication(authHeader, supervisorIdListString);
        }

        [HttpGet("vfoytd")]
        public async Task<IActionResult> VitalFewObjectivesYTD([FromServices] SAPClient client)
        {
            return Ok(await client.VitalFewObjectivesYTD(_user));
        }

        [HttpGet("mtdmaterialhpd")]
        public async Task<IActionResult> MTDMaterialHPD([FromServices] SAPClient client)
        {
            return Ok(await client.MTDMaterialHPD(_user));
        }

        [HttpGet("non-conformances")]
        public async Task<IActionResult> NonConformances([FromServices] SAPClient client)
        {
            return Ok(await client.NonConformances(_user));
        }

        [HttpGet("schindler-ahead")]
        public async Task<IActionResult> SchindlerAhead([FromServices] SAPClient client)
        {
            return Ok(await client.SchindlerAhead(_user));
        }

        [HttpGet("schindleraheaddetail/{title}")]
        public async Task<IActionResult> SchindlerAheadDetail([FromServices] SAPClient client, string title)
        {
            return Ok(await client.SchindlerAheadDetail(title, _user));
        }

        [HttpGet("/ossunits")]
        public async Task<IActionResult> OutOfServiceUnits([FromServices] SAPClient client)
        {
            return Ok(await client.OutOfServiceUnitsList(_user));
        }

        [HttpGet("/ossunitdetail/{equipmentNum}")]
        public async Task<IActionResult> UnitDetail([FromServices] SAPClient client, string equipmentNum)
        {
            return Ok(await client.OutOfServiceUnitDetail(equipmentNum, _user));
        }

        [HttpPost("/ossunit/{equipmentNum}/ReturnToService")]
        public async Task<IActionResult> ReturnToService([FromServices] SAPClient client, string equipmentNum, [FromBody] string note)
        {
            return Ok(await client.ReturnToService(equipmentNum, note, _user));
        }

        [HttpPost("/ossunit/{equipmentNum}/AddComment")]
        public async Task<IActionResult> AddComment([FromServices] SAPClient client, string equipmentNum, [FromBody] string note)
        {
            return Ok(await client.AddComment(equipmentNum, note, _user));
        }

        [HttpGet("/sickunits")]
        public async Task<IActionResult> SickUnits([FromServices] SAPClient client)
        {
            return Ok(await client.SickUnits(_user));
        }

        [HttpGet("/lingeringopencalls")]
        public async Task<IActionResult> LingeringOpenCalls([FromServices] SAPClient client)
        {
            return Ok(await client.LingeringOpenCalls(_user));
        }

        [HttpGet("/nonconformancemodaldetail/{title}")]
        public async Task<IActionResult> NonConformanceDetail([FromServices] SAPClient client, string title)
        {
            var result = await client.NonConformanceDetail(title, _user);
            return base.Ok(result);
        }
        
    }
}
