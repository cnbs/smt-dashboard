using System.Net;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using smt_dashboard.ViewModels;

namespace smt_dashboard.Controllers
{
    [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
    public class DashboardController : Controller
    {
        private readonly IConfiguration _config;

        public DashboardController(IConfiguration config)
        {
            _config = config;
        }

        [Route("")]
        public IActionResult Index([FromQuery] bool showlogin)
        {
            ViewData["NONCE"] = HttpContext.Items["NONCE"];
            DashboardViewModel model = new DashboardViewModel
            {
                ShowLoginPage = showlogin,// showlogin, //true,
                SAPServer = _config["APIConfig:BaseUrl"],
                AppId = _config["APIConfig:apiid"],
                BuildNumber = typeof(DashboardController).Assembly.GetName().Version.ToString(),
                Base = _config["UrlBase"]
            };
            return base.View(model);
        }

        [Route("/order")]
        public IActionResult MyOrder()
        {
            ViewData["NONCE"] = HttpContext.Items["NONCE"];
            return View();
        }

        [Route("/selectequipment")]
        public IActionResult SelectEquipment()
        {
            ViewData["NONCE"] = HttpContext.Items["NONCE"];
            return View();
        }
    }
}