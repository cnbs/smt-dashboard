using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using smt_dashboard.Models;
using smt_dashboard.ViewModels;

namespace smt_dashboard.Controllers
{
    [Route("catalog")]
    [ResponseCache(NoStore =true, Location =ResponseCacheLocation.None)]
    public class CatalogController : Controller
    {
        private IConfiguration _config;

        public CatalogController(IConfiguration config)
        {
            _config = config;
        }

        [Route("{*page}")]
        public IActionResult Index()
        {
            ViewData["NONCE"] = HttpContext.Items["NONCE"];
            DashboardViewModel model = new DashboardViewModel
            {
                SAPServer = _config["APIConfig:BaseUrl"],
                AppId = _config["APIConfig:apiid"],
                BuildNumber = typeof(DashboardController).Assembly.GetName().Version.ToString(),
                CatalogUrl = _config["CatalogAPiConfig:CatalogUrl"],
                Base = _config["UrlBase"]
            };
            return View(model);
        }
        
        [HttpPut("/api/checkout-details")]
        public async Task<IActionResult> CheckoutDetails([FromServices] ICatalogService catalog, [FromBody] Dictionary<string, int> products)
        {
            try
            {

                // We want to send one HTTP Request first to so we don't accidently 
                var techniciansTask = catalog.GetShipTos();
                var technicians = await techniciansTask;
                
                var shippingTypesTask = catalog.GetShippingTypes();
                var productDetailsTask = catalog.GetProductDetails(products.Keys);
                var userDetailTask = catalog.GetUserDetail();
                var shippingTypes = await shippingTypesTask;
                var productDetails = await productDetailsTask;
                var userDetail = await userDetailTask;
                
                var orderItems = products.Keys.Select(key =>
                {
                    if (productDetails.ContainsKey(key))
                    {
                        return new OrderItem
                        {
                            Product = productDetails[key],
                            Quantity = products[key],
                        };
                    }

                    return new OrderItem
                    {
                        Product = new ProductDetail
                        {
                            Currency = "",
                            Name = "Error: Could not find product.",
                            UnitPrice = 0.0,
                        },
                        Quantity = products[key],
                    };
                });
                
                return base.Ok(new CheckoutDetails
                {
                    ShipTos = technicians,
                    ShippingTypes = shippingTypes,
                    OrderItems = orderItems,
                    UserDetail = userDetail,
                });
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
        
        [HttpGet("/api/building/{buildingNumber}/equipment")]
        public async Task<IActionResult> EquipmentSearch([FromServices] ICatalogService catalog, string buildingNumber)
        {
            IEnumerable<Equipment> equipment = await catalog.GetEquipment(buildingNumber);

            return base.Ok(equipment);
        }
        
        [HttpGet("/api/building-search")]
        public async Task<IActionResult> BuildingSearch([FromServices] ICatalogService catalog, [FromQuery] string name, [FromQuery] string searchType)
        {
            var equipment = await catalog.SearchBuilding(name, searchType);

            return base.Ok(equipment);
        }
        
        [HttpGet("/api/order-history")]
        public async Task<IActionResult> OrderHistory([FromServices] ICatalogService catalog, [FromQuery] string from, [FromQuery] string to)
        {
            var fromDate = DateTime.Parse(from);
            var toDate = DateTime.Parse(to);
            
            var orders = await catalog.GetOrderHistory(fromDate, toDate);
            
            return base.Ok(orders);
        }
        
        [HttpGet("/api/order-detail")]
        public async Task<IActionResult> OrderDetail([FromServices] ICatalogService catalog, [FromQuery] string number)
        {
            Order order = await catalog.GetOrderDetails(number);
            
            return base.Ok(order);
        }

        [HttpPost("/api/submit-order")]
        public async Task<IActionResult> SubmitOrder([FromServices] ICatalogService catalog, [FromBody] NewOrder order)
        {
            var response = await catalog.SubmitOrder(order);
            
            return base.Ok(response);
        }

        [HttpGet("/api/charge")]
        public async Task<IActionResult> GetCharge([FromServices] ICatalogService catalog, [FromQuery] string equipment, [FromQuery] string userId)
        {
            // return Ok(new List<Visit_ORDER>
            // {
            //     new Visit_ORDER
            //     {
            //         VISIT = "5150574447",
            //         WORKDESC = "RANDOLPH T 1578 SUSSEX TPKE RANDOLPH 07869-1833",
            //         ESTHRSOLD = 2,
            //         PRICE = (decimal)3.000,
            //     }
            // });

            Visit response = await catalog.GetCharge(equipment, userId);
            
            return base.Ok(response.ORDERS);
        }
        
    }
}