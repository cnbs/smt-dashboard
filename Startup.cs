﻿using System;
using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using smt_dashboard.Exceptions;
using smt_dashboard.Interfaces;
using smt_dashboard.Middleware;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddHttpContextAccessor();
            services.AddTransient<HttpClient>();
            services.AddTransient<SAPClient>();
            services.AddTransient<ICatalogService, SapCatalogService>();
            services.AddTransient<IFmOrderService, SapFmOrderService>();
            services.AddMvc(options => options.EnableEndpointRouting = false).AddNewtonsoftJson(options =>
                options.SerializerSettings.ContractResolver = new DefaultContractResolver());
            services.AddSingleton<IAuthorizationHeaderService, AuthorizationHeaderService>();
            services.AddSingleton<ICachedTitles, CachedTitles>();

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedHost;
            });

            services.AddHsts(options =>
            {
                options.Preload = true;
                options.IncludeSubDomains = true;
                options.MaxAge = TimeSpan.FromDays(365);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseForwardedHeaders();

            }
            else
            {
                app.UseForwardedHeaders();
                app.UseHsts();
            }

            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {

                    var exceptionHandlerPathFeature =
                        context.Features.Get<IExceptionHandlerPathFeature>();

                    // Use exceptionHandlerPathFeature to process the exception (for example, 
                    // logging), but do NOT expose sensitive error information directly to 
                    // the client.

                    if (exceptionHandlerPathFeature?.Error is SapDataException)
                    {
                        context.Response.ContentType = "application/json";
                        context.Response.StatusCode = 400;
                        var exception = exceptionHandlerPathFeature.Error;
                        var result = JsonConvert.SerializeObject(new
                            {error = exception.Message});
                        context.Response.ContentType = "application/json";

                        await context.Response.WriteAsync(result);
                    }
                    else
                    {
                        context.Response.ContentType = "application/json";
                        context.Response.StatusCode = 500;
                        var exception = exceptionHandlerPathFeature?.Error;
                        var result = JsonConvert.SerializeObject(new
                            {error = exception?.Message});
                        context.Response.ContentType = "application/json";
                        await context.Response.WriteAsync(result);
                        
                    }
                });
            });
            
            
            app.UseCsp(builder =>
            {
            
                
                builder.Defaults
                    .AllowSelf();

                builder.Scripts
                    .AllowSelf()
                    .Allow("https://ajax.googleapis.com")
                    .Allow("https://cdnjs.cloudflare.com/ajax/libs/popper.js/")
                    .Allow("https://maxcdn.bootstrapcdn.com")
                    .Allow("https://use.fontawesome.com")
                    .Allow("https://unpkg.com")
                    .Allow("https://prdapp02.xisecurenet.com");

                builder.Styles
                    .AllowSelf()
                    .Allow("'unsafe-inline'")
                    .Allow("https://use.fontawesome.com")
                    .Allow("https://fonts.googleapis.com");
            
                builder.Fonts
                    .AllowSelf()
                    .Allow("https://use.fontawesome.com")
                    .Allow("https://fonts.gstatic.com");
            
                builder.Images
                    .AllowSelf();
            
                builder.Connect
                    .AllowAny();
            });
            
            app.UseHttpsRedirection();
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                //context.Response.Headers.Add("Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload");
                context.Response.Headers.Add("X-Frame-Options", "DENY");
                context.Response.Cookies.Append("Session_Cookie", DateTime.Now.ToString(), new CookieOptions { HttpOnly = true });
                await next();
            });
            app.UseStaticFiles("/wwwroot");
            app.UseMvc();
        }
    }
}