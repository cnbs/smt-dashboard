namespace smt_dashboard
{
    public class LabelValuePairDTO
    {
        public LabelValuePairDTO(string label, string value)
        {
            Label = label;
            Value = value;
        }
        public string Label { get; set; }
        public string Value { get; set; }
    }
}
