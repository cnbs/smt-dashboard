namespace smt_dashboard
{
    public class LabelValuePairListBaseDTO : DashboardWidgetBaseDTO
    {
        public LabelValuePairListBaseDTO(string title, LabelValuePairDTO[] items)
        {
            Title = title;
            Items = items;
        }

        public LabelValuePairListBaseDTO(string title, string helpText, LabelValuePairDTO[] items)
        {
            Title = title;
            HelpText = helpText;
            Items = items;
        }

        public LabelValuePairDTO[] Items { get; set; }
    }
}
