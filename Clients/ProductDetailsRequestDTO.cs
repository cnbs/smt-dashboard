using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class ProductDetailsRequestDTO
    {
        [JsonProperty("parts")]
        public IEnumerable<ProductDetailsRequestPartDTO> Parts = new List<ProductDetailsRequestPartDTO>();
    }

    public class ProductDetailsRequestPartDTO
    {
        [JsonProperty("MATNR")]
        public string MaterialNumber;
    }
    
}
