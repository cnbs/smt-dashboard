using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class ShipTypeDTO
    {
        [JsonProperty("HEADER")]
        public HeaderDTO Header { get; set; }

        [JsonProperty("DETAIL")]
        public ShipTypeListDTO Detail { get; set; }
    }

    public class ShipTypeListDTO
    {
        [JsonProperty("SHIPTYPES")]
        public IEnumerable<ShipTypeDTODetail> ShipTypes { get; set; }
        
    } 


    public class ShipTypeDTODetail
    {
        [JsonProperty("DEL_PRIO")]
        public string Code { get; set; }

        [JsonProperty("DEL_PRIO_DESC")]
        public string Name { get; set; }
        
        [JsonProperty("EMAIL_REQD")]
        public string EmailRequired { get; set; }
        

    }
}