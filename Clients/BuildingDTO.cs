using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class BuildingDTO
    {
        [JsonProperty("HEADER")]
        public BuildingDTOHeader Detail { get; set; }

        [JsonProperty("DETAIL")]
        public BuildingListDTO Details { get; set; }
    }

    public class BuildingListDTO
    {
        [JsonProperty("BUILDING")]
        public IEnumerable<BuildingDTODetail> Building { get; set; }
    }

    public class BuildingDTOHeader
    {
        [JsonProperty("TITLE")]
        public string Title { get; set; }

        [JsonProperty("HELP_TEXT")]
        public string HelpText { get; set; }
    }

    public class BuildingDTODetail
    {
        [JsonProperty("BUILD_NUM")]
        public string Number { get; set; }

        [JsonProperty("BUILD_DESC")]
        public string Description { get; set; }

        [JsonProperty("STREET")]
        public string Street { get; set; }
        
        [JsonProperty("CITY")]
        public string City { get; set; }
    }
}