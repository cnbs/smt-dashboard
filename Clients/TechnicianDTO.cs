﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class TechnicianDTO
    {
        [JsonProperty("HEADER")]
        public HeaderDTO Header { get; set; }

        [JsonProperty("DETAIL")]
        public TechnicianListDTO Detail { get; set; }
    }

    public class TechnicianListDTO
    {
        [JsonProperty("OFFICETECHS")]
        public IEnumerable<TechnicianDTODetail> Technicians { get; set; }
    }

    public class TechnicianDTODetail
    {
        [JsonProperty("PERNR")]
        public string PersonnelNumber { get; set; }

        [JsonProperty("VNAMC")]
        public string FirstName { get; set; }

        [JsonProperty("NCHMC")]
        public string LastName { get; set; }
    }
}