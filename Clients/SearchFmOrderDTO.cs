﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class SearchFmOrderDTO
    {
        [JsonProperty("ORDERS")]
        public List<SearchFmOrder_ORDER> ORDERS { get; set; }
    }

    public class SearchFmOrder_ORDER
    {
        [JsonProperty("VBELN")]
        public string VBELN { get; set; }

        [JsonProperty("REPEVTID")]
        public int REPEVTID { get; set; }

        [JsonProperty("DESCR")]
        public string DESCR { get; set; }

        //[JsonProperty("ENAME")]
        //public string ENAME { get; set; }

        [JsonProperty("EST_LAB")]
        public decimal ESTLAB { get; set; }

        [JsonProperty("EST_MAT")]
        public decimal ESTMAT { get; set; }

        [JsonProperty("FIELDINSTR")]
        public string FIELDINSTR { get; set; }

        [JsonProperty("COMMITDATE")]
        public string COMMITDATE { get; set; }

        [JsonProperty("COMMIT_TIME")]
        public string COMMITTIME { get; set; }

        [JsonProperty("TECH1")]
        public string TECH1 { get; set; }

        [JsonProperty("TECH2")]
        public string TECH2 { get; set; }

        [JsonProperty("TECH1_PERNR")]
        public string TECH1_PERNR { get; set; }

        [JsonProperty("TECH2_PERNR")]
        public string TECH2_PERNR { get; set; }
    }

    public class STATUS
    {
        [JsonProperty("MSGTY")]
        public string MSGTY { get; set; }

        [JsonProperty("MSGID")]
        public string MSGID { get; set; }

        [JsonProperty("MSGNO")]
        public int MSGNO { get; set; }

        [JsonProperty("MSGLN")]
        public string MSGLN { get; set; }

        [JsonProperty("JSON")]
        public string JSON { get; set; }
    }
}