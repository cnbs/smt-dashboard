using Newtonsoft.Json;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public class NewOrderResponseDTO
    {
        [JsonProperty("HEADER")]
        public HeaderDTO Header { get; set; }

        [JsonProperty("DETAIL")]
        public NewOrderResponseDetailDTO Detail { get; set; }
    }

    public class NewOrderResponseDetailDTO
    {
        [JsonProperty("ORDER_NUMBER")]
        public string OrderNumber { get; set; }
    }
}