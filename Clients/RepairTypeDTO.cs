﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class RepairTypeDTO
    {
        [JsonProperty("HEADER")]
        public HeaderDTO Header { get; set; }

        [JsonProperty("DETAIL")]
        public RepairTypeListDTO Detail { get; set; }
    }

    public class RepairTypeListDTO
    {
        [JsonProperty("REPAIRTYPES")]
        public IEnumerable<RepairTypeDTODetail> RepairTypes { get; set; }
    }

    public class RepairTypeDTODetail
    {
        [JsonProperty("REPAIR_CODE")]
        public string Code { get; set; }

        [JsonProperty("DESCR")]
        public string Name { get; set; }

        [JsonProperty("CLASS")]
        public string Class { get; set; }

    }
}