using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class ProductDetailDTO
    {
        
        [JsonProperty("HEADER")]
        public HeaderDTO Detail { get; set; }

        [JsonProperty("DETAIL")]
        public ProductDetailDTOProductSet Details { get; set; }
    }

    public class ProductDetailDTOProductSet
    {
        [JsonProperty("PRODUCT")]
        public IEnumerable<ProductDetailDTODetail> Products { get; set; }
    }


    public class ProductDetailDTODetail
    {
        [JsonProperty("MATNR")]
        public string Number { get; set; }

        [JsonProperty("DESC")]
        public string Name { get; set; }

        [JsonProperty("COST")]
        public double UnitPrice { get; set; }
        
        [JsonProperty("CURR")]
        public string Currency { get; set; }
        
        [JsonProperty("UOM")]
        public string UOM { get; set; }
    }
}