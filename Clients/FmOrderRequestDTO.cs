﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public class FmOrderRequestDTO
    {
        [JsonProperty("EQUIPMENTNUM")]
        public string EquipmentNumber { get; set; }

        [JsonProperty("REPAIR_CODE")]
        public string RepairCode { get; set; }

        [JsonProperty("FIELD_INSTR")]
        public string FieldInstructions { get; set; }

        [JsonProperty("EST_LABOR_HRS")]
        public string EstLaborHours { get; set; }

        [JsonProperty("EST_MATR_COST")]
        public string EstMaterialCost { get; set; }

        [JsonProperty("WAERS")]
        public string Currency { get; set; }

        [JsonProperty("PERNR")]
        public string PersonnelNumber { get; set; }

        [JsonProperty("START_DATE")] //YYYYMMDD
        public string StartDate { get; set; }

        [JsonProperty("TECH1")]
        public string Tech1 { get; set; }

        [JsonProperty("TECH2")]
        public string Tech2 { get; set; }
    }

}