using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public class NewOrderRequestDTO
    {
        [JsonProperty("ShipTo")]
        public string ShipToNumber { get; set; }

        [JsonProperty("EQUIPMENT")]
        public string EquipmentNumber { get; set; }

        [JsonProperty("PRIORITY")]
        public string PriorityNumber { get; set; }

        [JsonProperty("NOTIFYEMAIL")]
        public string NotifyEmail { get; set; }

        [JsonProperty("PRODUCT")]
        public IEnumerable<NewOrderLineItemDTO> LineItems { get; set; } = new List<NewOrderLineItemDTO>();

        [JsonProperty("ATTENTION")]
        public string Attention { get ; set ; }

        [JsonProperty("CHARGENO")]
        public string ChargeNumber { get ; set ; }
    }

    public class NewOrderLineItemDTO
    {
        [JsonProperty("MATNR")]
        public string MaterialNumber { get; set; }

        [JsonProperty("QUANTITY")]
        public int Quantity { get; set; }
    }
}