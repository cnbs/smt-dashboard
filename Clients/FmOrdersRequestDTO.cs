﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class FmOrdersRequestDTO
    {
        [JsonProperty("para")]
        public List<Para> Para { get; set; }
    }

    public class Para
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

}