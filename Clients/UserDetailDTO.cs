using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class UserDetailResponseDTO
    {
        [JsonProperty("HEADER")]
        public HeaderDTO Header { get; set; }

        [JsonProperty("DETAIL")]
        public UserDetailDTO Details { get; set; }
    }

    public class UserDetailDTO
    {
        [JsonProperty("EMAILADDR")]
        public string EmailAddress { get; set; }
        
        [JsonProperty("CANORDER")]
        public string CanOrder { get; set; }
    }
}