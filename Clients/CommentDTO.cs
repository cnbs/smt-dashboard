using System;

namespace smt_dashboard
{
    public class CommentDTO
    {
        public CommentDTO(string comment)
        {
            Comment = comment;
        }
        public string Comment { get; set; }
    }
}
