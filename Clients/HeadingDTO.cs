namespace smt_dashboard
{
    public class HeadingDTO
    {
        public HeadingDTO(string label)
        {
            Label = label;
        }
        public string Label { get; set; }
    }
}
