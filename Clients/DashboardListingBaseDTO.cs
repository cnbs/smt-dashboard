﻿using System.Linq;

namespace smt_dashboard
{
    public class DashboardListingBaseDTO : DashboardWidgetBaseDTO
    {
        public DashboardListingBaseDTO(string title)
        {
            Title = title;
        }

        public DashboardListingBaseDTO(string title, string helpText, dynamic[] items, params string[] headings)
        {
            Title = title;
            HelpText = helpText;
            Headings = headings.Select(h => new HeadingDTO(h)).ToArray();
            Items = items;
        }

        public DashboardListingBaseDTO(string title, string parentTitle, string helpText, dynamic[] items, params string[] headings)
        {
            Title = title;
            ParentTitle = parentTitle;
            HelpText = helpText;
            Headings = headings.Select(h => new HeadingDTO(h)).ToArray();
            Items = items;
        }

        public DashboardListingBaseDTO(string title, string helpText, HeadingDTO[] headings, dynamic[] items)
        {
            Title = title;
            HelpText = helpText;
            Headings = headings;
            Items = items;
        }

        public DashboardListingBaseDTO(string title, string parentTitle, string helpText, HeadingDTO[] headings, dynamic[] items)
        {
            Title = title;
            ParentTitle = parentTitle;
            HelpText = helpText;
            Headings = headings;
            Items = items;
        }
        public HeadingDTO[] Headings { get; set; } = new HeadingDTO[] { };
        public dynamic[] Items { get; set; } = new dynamic[] { };
    }
}
