using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using smt_dashboard.Interfaces;
using smt_dashboard.Models;

using System.Data;

namespace smt_dashboard
{
    public class SAPClient
    {
        private readonly IAuthorizationHeaderService _authService;
        private readonly ILogger<SAPClient> _logger;
        private readonly CachedTitles _titles;
        private HttpClient _client;
        private readonly string _apiId;
        private readonly string _sap_client;
        private readonly string _default_params;

        public SAPClient(HttpClient client, IConfiguration config, IAuthorizationHeaderService authService, ILogger<SAPClient> logger, ICachedTitles titles)
        {
            _authService = authService;
            _logger = logger;
            _titles = (CachedTitles)titles;
            _client = client;
            _client.BaseAddress = new Uri(config["APIConfig:BaseUrl"]);
            _client.DefaultRequestHeaders.Add("cnbssysid", config["APIConfig:cnbssysid"]);
            _apiId = config["APIConfig:apiid"];
            _sap_client = config["APIConfig:sap-client"];
            _default_params = $"sap-client={_sap_client}&apiid={_apiId}";
        }

        public async Task<LabelValuePairListBaseDTO> VitalFewObjectivesYTD(AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, "vfoytd");
                var definition = new { DATA = new { HEADER = new { TITLE = "", HELP_TEXT = "" }, DETAIL = new[] { new { ATTRIBUTE_LABEL = "", ATTRIBUTE_VALUE = "" } } } };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var header = obj.DATA.HEADER;
                var items = obj.DATA;
                _titles.VitalFewObjectivesYTDTitle = header.TITLE;
                return new LabelValuePairListBaseDTO(header.TITLE, header.HELP_TEXT, items.DETAIL.Select(d => new LabelValuePairDTO(d.ATTRIBUTE_LABEL, d.ATTRIBUTE_VALUE)).ToArray());
            }
            catch (System.Exception ex)
            {
                return new LabelValuePairListBaseDTO($"Error: {ex.Message}", string.Empty, new LabelValuePairDTO[] { });
            }
        }

        public async Task<DashboardListingBaseDTO> OutOfServiceUnitsList(AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, "ossunits");
                var definition = new { DATA = new { HEADER = new { TITLE = "", HELP_TEXT = "" }, DETAIL = new[] { new { BUILDINGNAME = "", DAYSOOS = 0, DISTRICT = "", EQUIPMENTDESC = "", EQUIPMENTNO = "", OSSCOMMENT = "", SUPERINTENDENT = "" } } } };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var items = obj.DATA;
                var header = items.HEADER;
                _titles.OutOfServiceUnitsTitle = header.TITLE;
                return new DashboardListingBaseDTO($"{header.TITLE} ({items?.DETAIL?.Length ?? 0})", header.HELP_TEXT, items?.DETAIL?.Select(d => new
                {
                    BuildingName = d.BUILDINGNAME,
                    EquipmentDesc = d.EQUIPMENTDESC,
                    EquipmentNumber = d.EQUIPMENTNO,
                    DaysOutOfService = d.DAYSOOS,
                    LatestComment = d.OSSCOMMENT ?? string.Empty
                }).ToArray(), "Building", "Equipment", "Days OOS", "Last Comment");
            }
            catch (System.Exception ex)
            {
                return new DashboardListingBaseDTO($"Error: {ex.Message}");
            }
        }

        public async Task<DashboardListingBaseDTO> SchindlerAheadDetail(string title, AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, $"secahead/{title}");
                var definition = new { DATA = new { HEADER = new { TITLE = "", HELP_TEXT = "" }, DETAIL = new[] { new { BUILDINGNAME = "", EQUIPMENTDESC = "", CONTRACTNUMBER = "" } } } };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var items = obj.DATA;
                var header = items.HEADER;
                return new DashboardListingBaseDTO(title, _titles.SchindlerAheadTitle, header.HELP_TEXT, items?.DETAIL?.Select(b => new
                {
                    BuildingName = b.BUILDINGNAME,
                    EquipmentDesc = b.EQUIPMENTDESC,
                    ContractNumber = b.CONTRACTNUMBER
                }).ToArray(), "Building","Contract Number", "Equipment");
            }
            catch (System.Exception ex)
            {
                return new DashboardListingBaseDTO($"Error: {ex.Message}");
            }
        }

        public async Task<UnitDetailDTO> ReturnToService(string equipmentNum, string note, AuthenticatedUser user)
        {
            try
            {
                var res = SAPCommand(user, $"ossunits/{equipmentNum}/ReActivate", $"&ID={equipmentNum}", HttpMethod.Post, $"{{\"NOTE\":\"{note}\"}}").Result;
                var freshData = await OutOfServiceUnitDetail(equipmentNum, user);
                return freshData;
            }
            catch (System.Exception ex)
            {
                return new UnitDetailDTO { Unit = $"Error: {ex.Message}" };
            }
        }

        public async Task<UnitDetailDTO> AddComment(string equipmentNum, string note, AuthenticatedUser user)
        {
            try
            {
                var res = SAPCommand(user, $"ossunits/{equipmentNum}/AddNote", $"&ID={equipmentNum}", HttpMethod.Post, $"{{\"NOTE\":\"{note}\"}}").Result;
                var freshData = await OutOfServiceUnitDetail(equipmentNum, user);
                return freshData;
            }
            catch (System.Exception ex)
            {
                return new UnitDetailDTO { Unit = $"Error: {ex.Message}" };
            }
        }

        public async Task<DashboardListingBaseDTO> NonConformanceDetail(string title, AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, $"nonconformances/{title}");
                var definition = new { DATA = new { HEADER = new { TITLE = "", HELP_TEXT = "" }, DETAIL = new[] { new { BUILDINGNAME = "", DEFICIENCY = "", EQUIPMENTDESC = "" } } } };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var items = obj.DATA;
                var header = items.HEADER;
                var result = new DashboardListingBaseDTO(title, _titles.NonConformancesTitle, header.HELP_TEXT, new HeadingDTO[] {
                    new HeadingDTO("Building"),
                    new HeadingDTO("Equipment Description"),
                    new HeadingDTO("Deficiency")
                }, items?.DETAIL?.Select(d => new
                {
                    BuildingName = d.BUILDINGNAME,
                    Deficiency = d.DEFICIENCY ?? string.Empty,
                    EquipmentDesc = d.EQUIPMENTDESC
                }).ToArray());
                return result;
            }
            catch (System.Exception ex)
            {
                return new DashboardListingBaseDTO($"Error: {ex.Message}", string.Empty, string.Empty, new HeadingDTO[] { }, new dynamic[] { });
            }
        }

        public async Task<LabelValuePairListBaseDTO> NonConformances(AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, "nonconformances");
                var definition = new { DATA = new { HEADER = new { TITLE = "", HELP_TEXT = "" }, DETAIL = new[] { new { ATTRIBUTE_LABEL = "", ATTRIBUTE_VALUE = "" } } } };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var items = obj.DATA;
                var header = items.HEADER;
                int count = items?.DETAIL?.Sum(i => int.TryParse(i.ATTRIBUTE_VALUE, out int x) ? x : 0) ?? 0;
                _titles.NonConformancesTitle = header.TITLE;
                return new LabelValuePairListBaseDTO($"{header.TITLE} ({count})", header.HELP_TEXT, items?.DETAIL?.Select(d => new LabelValuePairDTO(d.ATTRIBUTE_LABEL, d.ATTRIBUTE_VALUE)).ToArray());
            }
            catch (System.Exception ex)
            {
                return new LabelValuePairListBaseDTO($"Error: {ex.Message}", string.Empty, new LabelValuePairDTO[] { });
            }
        }

        public async Task<LabelValuePairListBaseDTO> MTDMaterialHPD(AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, "mtdmaterialhpd");
                var definition = new { DATA = new { HEADER = new { TITLE = "", HELP_TEXT = "" }, DETAIL = new[] { new { ATTRIBUTE_LABEL = "", ATTRIBUTE_VALUE = "" } } } };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var items = obj.DATA;
                var header = items.HEADER;
                _titles.MTDMaterialHPDTitle = header.TITLE;
                return new LabelValuePairListBaseDTO($"{header.TITLE}", header.HELP_TEXT, items?.DETAIL?
                    .Select(d => new LabelValuePairDTO(d.ATTRIBUTE_LABEL, d.ATTRIBUTE_VALUE))
                    .ToArray());
            }
            catch (System.Exception ex)
            {
                return new LabelValuePairListBaseDTO($"Error: {ex.Message}", new LabelValuePairDTO[] { });
            }
        }

        public async Task<LabelValuePairListBaseDTO> SchindlerAhead(AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, "secahead");
                var definition = new { DATA = new { HEADER = new { TITLE = "", HELP_TEXT = "" }, DETAIL = new[] { new { ATTRIBUTE_LABEL = "", ATTRIBUTE_VALUE = "" } } } };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var items = obj.DATA;
                var header = items.HEADER;
                _titles.SchindlerAheadTitle = header.TITLE;
                return new LabelValuePairListBaseDTO($"{header.TITLE}", header.HELP_TEXT, items?.DETAIL?
                    .Select(d => new LabelValuePairDTO(d.ATTRIBUTE_LABEL, d.ATTRIBUTE_VALUE))
                    .ToArray());
            }
            catch (Exception ex)
            {
                return new LabelValuePairListBaseDTO($"Error: {ex.Message}", new LabelValuePairDTO[] { });
            }
        }

        public async Task<UnitDetailDTO> OutOfServiceUnitDetail(string equipmentNum, AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, $"ossunits/{equipmentNum}");
                var definition = new
                {
                    DATA = new
                    {
                        HEADER = new { TITLE = "", HELP_TEXT = "" },
                        DETAIL = new { SUPERINTENDENT = "", DISTRICT = "", BUILDINGNAME = "", EQUIPMENTDESC = "", DAYSOOS = 0, OSSCOMMENT = "", DOWNBYTECH = "", MAINTTECH = "", DESCRIPTION = "", EQUIP_NOTES = new[] { new { EQUIPMENTNO = "", NOTE = "" } } },
                        STATUS = new { MSGTY = "", MSGID = "", MSGNO = 0, MSGLN = "" }
                    }
                };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var item = obj.DATA;
                var header = item.HEADER;
                var retval = new UnitDetailDTO
                {
                    ParentTitle = _titles.OutOfServiceUnitsTitle,
                    Unit = item?.DETAIL?.EQUIPMENTDESC,
                    BuildingName = item?.DETAIL?.BUILDINGNAME,
                    DownByTech = item?.DETAIL?.DOWNBYTECH,
                    MaintenanceTech = item?.DETAIL?.MAINTTECH,
                    DaysOutOfService = item?.DETAIL?.DAYSOOS ?? 0,
                    EquipmentNumber = equipmentNum,
                    Comments = item?.DETAIL?.EQUIP_NOTES?.Select(n => new CommentDTO(n.NOTE)).ToArray()
                };
                return retval;
            }
            catch (System.Exception ex)
            {
                return new UnitDetailDTO { BuildingName = $"Error: {ex.Message}" };
            }
        }

        public async Task<DashboardListingBaseDTO> SickUnits(AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, "sickunits");
                var definition = new { DATA = new { HEADER = new { TITLE = "", HELP_TEXT = "" }, DETAIL = new[] { new { BUILDINGNAME = "", EQUIPMENTDESC = "", DOWNEDCAR = "" } } } };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var items = obj.DATA;
                var header = items.HEADER;
                _titles.SickUnitsTitle = header.TITLE;
                return new DashboardListingBaseDTO($"{header.TITLE} ({items?.DETAIL?.Length ?? 0})", header.HELP_TEXT, new HeadingDTO[]{
                    new HeadingDTO("Building"),
                    new HeadingDTO("Equipment"),
                    new HeadingDTO("Downed Car")
                }, items?.DETAIL?
                    .Select(d => new
                    {
                        BuildingName = d.BUILDINGNAME,
                        EquipmentDesc = d.EQUIPMENTDESC,
                        DownedCar = d.DOWNEDCAR ?? string.Empty
                    })
                    .ToArray());
            }
            catch (Exception ex)
            {
                return new DashboardListingBaseDTO($"Error: {ex.Message}", string.Empty, string.Empty, new HeadingDTO[] { }, new dynamic[] { });
            }
        }

        public async Task<DashboardListingBaseDTO> LingeringOpenCalls(AuthenticatedUser user)
        {
            try
            {
                var res = await SAPCommand(user, "opencalls");
                var definition = new { DATA = new { HEADER = new { TITLE = "", HELP_TEXT = "" }, DETAIL = new[] { new { BUILDINGNAME = "", EQUIPMENTDESC = "", CALL_ENTERED = "", NOTIFICATIONNO = "" } } } };
                var obj = JsonConvert.DeserializeAnonymousType(res, definition);
                var items = obj.DATA;
                var header = items.HEADER;
                _titles.LingeringOpenCallsTitle = header.TITLE;
                return new DashboardListingBaseDTO($"{header.TITLE} ({items?.DETAIL?.Length ?? 0})", header.HELP_TEXT, new HeadingDTO[]{
                    new HeadingDTO("Building"),
                    new HeadingDTO("Equipment"),
                    new HeadingDTO("Call Time")
                }, items?.DETAIL?
                    .Select(d => new
                    {
                        BuildingName = d.BUILDINGNAME,
                        EquipmentDesc = d.EQUIPMENTDESC,
                        CallTime = d.CALL_ENTERED ?? string.Empty
                    })
                    .ToArray());
            }
            catch (System.Exception ex)
            {
                return new DashboardListingBaseDTO($"Error: {ex.Message}", string.Empty, new HeadingDTO[] { }, new dynamic[] { });
            }
        }

        private async Task<string> SAPCommand(AuthenticatedUser user, string commandName, string paramList = null, HttpMethod method = null, string content = null)
        {
            try
            {
                _logger.LogInformation($"Login Info: User: {user.Username}, Super list: {user.SupervisorIdListString}");
                var userParams = $"&USERNAME={user.Username}&SUPERINTENDENTS={user.SupervisorIdListString}";
                var url = new Uri($"{commandName}?{_default_params}{userParams}{(paramList == null ? "" : $"&{paramList}")}", UriKind.Relative);
                _logger.LogInformation($"++++++++++ {url}");
                _client.DefaultRequestHeaders.Remove("Authorization");
                _client.DefaultRequestHeaders.Add("Authorization", user.AuthorizationHeader);
                _client.DefaultRequestHeaders.Remove("cache-control");
                _client.DefaultRequestHeaders.Add("cache-control", "no-cache");
                HttpResponseMessage res;
                if (method == HttpMethod.Post)
                {
                    res = await _client.PostAsync(url, new StringContent(content));
                }
                else
                {
                    res = await _client.GetAsync(url);
                }
                _logger.LogInformation($"Response Info: Status: {res.StatusCode}, Reason: {res.ReasonPhrase}");
                if (res.StatusCode != System.Net.HttpStatusCode.OK) throw new Exception($"SAP ({res.ReasonPhrase})");
                res.EnsureSuccessStatusCode();
                var textResult = await res.Content.ReadAsStringAsync();
                _logger.LogInformation($">>>>>>> SAP GET result: {textResult}");
                return textResult.Contains("DATA") ? textResult : $"{textResult.Insert(1, $"DATA: {{ HEADER: {{ TITLE: \"Not Implemented Yet\", HELP_TEXT: \"Not Implemented Yet\" }},")} }}";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Generic Exception Handler");
                throw;
            }
        }
    }
}
