namespace smt_dashboard
{
    public class DashboardWidgetBaseDTO
    {
        public string Title { get; set; }
        public string ParentTitle { get; set; }
        public string HelpText { get; set; }
    }
}
