namespace smt_dashboard
{
    public class UnitDetailDTO
    {
        public string Unit { get; set; }
        public string BuildingName { get; set; }
        public string DownByTech { get; set; }
        public int DaysOutOfService { get; set; }
        public string MaintenanceTech { get; set; }
        public CommentDTO[] Comments { get; set; }
        public string EquipmentNumber { get; set; }
        public string ParentTitle { get; set; }
    }
}
