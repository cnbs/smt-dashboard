using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class EquipmentDTO
    {
        [JsonProperty("HEADER")]
        public EquipmentDTOHeader Detail { get; set; }

        [JsonProperty("DETAIL")]
        public EquipmentListDTO Details { get; set; }
    }

    public class EquipmentListDTO
    {
        [JsonProperty("EQUIPMENT")]
        public IEnumerable<EquipmentDTODetail> Equipment { get; set; }
    }

    public class EquipmentDTOHeader
    {
        [JsonProperty("TITLE")]
        public string Title { get; set; }

        [JsonProperty("HELP_TEXT")]
        public string HelpText { get; set; }
    }

    public class EquipmentDTODetail
    {
        [JsonProperty("EQUNR")]
        public string Number { get; set; }

        [JsonProperty("EQUIP_DESC")]
        public string Description { get; set; }

        [JsonProperty("ADDRESS")]
        public string Address { get; set; }

        [JsonProperty("PERNR")]
        public string PersonnelNumber { get; set; }

        [JsonProperty("VKBUR")]
        public string OfficeId { get; set; }

        [JsonProperty("VNAMC")]
        public string FirstName { get; set; }

        [JsonProperty("NCHMC")]
        public string LastName { get; set; }

        [JsonProperty("ATTENTION")]
        public string Attention { get; set; }

    }
}