using Newtonsoft.Json;

namespace smt_dashboard
{
    public class HeaderDTO
    {
        [JsonProperty("TITLE")]
        public string Title { get; set; }

        [JsonProperty("HELP_TEXT")]
        public string HelpText { get; set; }
    }
}