using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard
{
    public class ShipToDTO
    {
        [JsonProperty("HEADER")]
        public HeaderDTO Header { get; set; }

        [JsonProperty("DETAIL")]
        public IEnumerable<ShipToDTODetail> Detail { get; set; }
    }



    public class ShipToDTODetail
    {
        [JsonProperty("CUSTOMER")]
        public string Number { get; set; }

        [JsonProperty("NAME")]
        public string Name { get; set; }
        
        [JsonProperty("STREET")]
        public string Street { get; set; }
        
        [JsonProperty("CITY")]
        public string City { get; set; }
    }
}