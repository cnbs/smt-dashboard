﻿using Newtonsoft.Json;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public class FmOrderResponseDTO
    {
        [JsonProperty("HEADER")]
        public HeaderDTO Header { get; set; }

        [JsonProperty("DETAIL")]
        public FmOrderResponseDetailDTO Detail { get; set; }
    }

    public class FmOrderResponseDetailDTO
    {
        [JsonProperty("ORDER_NUMBER")]
        public string OrderNumber { get; set; }
    }
}