using smt_dashboard.Models;

namespace smt_dashboard.Interfaces
{
    public interface IAuthorizationHeaderService
    {
        AuthenticatedUser GetAuthorizedUserFromHeaderAuthentication(string authHeader, string supervisorIdListString);
    }
}
