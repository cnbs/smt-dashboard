using System;

namespace smt_dashboard.Exceptions
{
    /**
     * We throw this exception when SAP returns json status of "E" 
     */
    public class SapDataException : Exception
    {
        public SapDataException()
        {
        }

        public SapDataException(string message)
            : base(message)
        {
        }

        public SapDataException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}