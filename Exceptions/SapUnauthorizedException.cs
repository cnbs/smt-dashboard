using System;

namespace smt_dashboard.Exceptions
{
    /**
     * Used when we have a communication error with SAPI, usually when we cannot reach the url or authenticate.
     */
    public class SapUnauthorized : Exception
    {
        public SapUnauthorized()
        {
        }

        public SapUnauthorized(string message)
            : base(message)
        {
        }

        public SapUnauthorized(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}