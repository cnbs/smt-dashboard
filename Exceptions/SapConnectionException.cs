using System;

namespace smt_dashboard.Exceptions
{
    /**
     * Used when we have a communication error with SAPI, usually when we cannot reach the url or authenticate.
     */
    public class SapConnectionException : Exception
    {
        public SapConnectionException()
        {
        }

        public SapConnectionException(string message)
            : base(message)
        {
        }

        public SapConnectionException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}