﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using smt_dashboard.Exceptions;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public class SapFmOrderService : IFmOrderService
    {
        protected readonly HttpClient _client;
        protected readonly IConfiguration _config;


        public SapFmOrderService(IHttpContextAccessor httpContextAccessor, IConfiguration config, HttpClient client)
        {
            string authHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            _config = config;
            _client = client;
            _client.DefaultRequestHeaders.Remove("Authorization");
            _client.DefaultRequestHeaders.Add("Authorization",
                authHeader);
            _client.DefaultRequestHeaders.Remove("cache-control");
            _client.DefaultRequestHeaders.Add("cache-control", "no-cache");
            _client.BaseAddress = new Uri(config["FMOrderAPIConfig:BaseUrl"]);
            _client.DefaultRequestHeaders.Add("cnbssysid", config["FMOrderAPIConfig:cnbssysid"]);
        }

        public async Task<IEnumerable<RepairType>> GetMockRepairTypes(string equipmentNumber)
        {
            return new List<RepairType>
            {
               new RepairType
               {
                Code = "12345",
                Name = "Repair Type 1",
                RepairClass = "Class 1",
               },
               new RepairType
               {
                Code = "12346",
                Name = "Repair Type 2",
                RepairClass = "Class 2",
               },
               new RepairType
               {
                Code = "12347",
                Name = "Repair Type 3",
                RepairClass = "Class 3",
               }
            };
        }

        //TODO: Figure out better solution or better endpoint
        public async Task<ResponseStatus> CheckLogin()
        {
            var url = BuildUrl("repairtypes",
                new Dictionary<string, string>
                    {{"equipmentnum", ""}}, useMock: false);

            var obj = await GetBasicResponseAsync<ResponseStatus>(url);

            return obj;
        }

        public async Task<IEnumerable<RepairType>> GetRepairTypes(string equipmentNumber)
        {
            var url = BuildUrl("repairtypes",
                new Dictionary<string, string>
                    {{"equipmentnum", equipmentNumber}}, useMock: false);

            var obj = await GetAsync<RepairTypeDTO>(url);

            return obj.Data.Detail.RepairTypes.Select(dto => new RepairType
            {
                Code = dto.Code,
                Name = dto.Name,
                RepairClass = dto.Class,
            });
        }

        public async Task<IEnumerable<Technician>> GetMockTechnicians(string officeId)
        {
            return new List<Technician>
            {
               new Technician
               {
                PersonnelNumber = "12345",
                FirstName = "Technician 1",
               },
               new Technician
               {
                PersonnelNumber = "12346",
                FirstName = "Technician 2 ",
               },
               new Technician
               {
                PersonnelNumber = "12347",
                FirstName = "Technician 3",
               }
            };
        }


        public async Task<IEnumerable<Technician>> GetTechnicians(string officeId)
        {
            var url = BuildUrl("officetechs",
                new Dictionary<string, string>
                    {{"OFFICEID", officeId}});

            var obj = await GetAsync<TechnicianDTO>(url);

            return obj.Data.Detail.Technicians.
            GroupBy(t => new {
                PersonnelNumber = t.PersonnelNumber,
            })
            .Select(dto => new Technician
            {
                PersonnelNumber = dto.First().PersonnelNumber,
                FirstName = dto.First().FirstName,
                LastName = dto.First().LastName,
            })
            .OrderBy(dto => dto.FirstName)
            .ThenBy(dto => dto.LastName)
            .ToList();
        }

        public async Task<IEnumerable<Building>> SearchBuilding(string name, string searchType)
        {
            string st;
            switch (searchType)
            {
                default:
                case "building": st = "I_BUILDING"; break;
                case "address":  st = "I_ADDRESS";  break;
                case "campus":   st = "I_CAMPUS";   break;
            }

            var url = BuildUrl("building",
                new Dictionary<string, string>
                    {{"BUILDINGNAME", name}, {st, "X"}});

            var obj = await GetAsync<BuildingDTO>(url);

            return obj.Data.Details.Building.Select(dto => new Building
            {
                Number = dto.Number,
                Street = dto.Street,
                City = dto.City,
                Description = dto.Description,
            });
        }

        public async Task<IEnumerable<FmOrder>> SearchFmOrders(string equipment, string officeId)
        {
            var url = BuildUrl("searchfmorder");

            var requestDTO = new FmOrdersRequestDTO
            {
                Para = new List<Para>
                {
                    new Para
                    {
                        Name  = "EQUIPNO", 
                        Value = equipment,
                    }
                }
            };

            var obj = await PostAsync<SearchFmOrderDTO>(url, requestDTO);

            var orders = obj.Data.ORDERS.Select(dto => new FmOrder
            {
                OrderNumber       = dto.VBELN,
                EquipmentNumber   = equipment,
                EstLaborHours     = dto.ESTLAB.ToString(CultureInfo.InvariantCulture),
                RepairCode        = dto.REPEVTID.ToString(CultureInfo.InvariantCulture),
                Description       = dto.DESCR,
                EstMaterialCost   = dto.ESTMAT.ToString(CultureInfo.InvariantCulture),
                FieldInstructions = dto.FIELDINSTR,
                Currency          = "USD",
                StartDate         = (DateTime.TryParseExact(dto.COMMITDATE,"yyyyMMdd", null, DateTimeStyles.None, out DateTime dt)?dt:default).ToString("yyyy-MM-dd"),
                PersonnelNumber   = dto.TECH1_PERNR,
                PersonnelNumber2  = dto.TECH2_PERNR,
                PersonnelName     = dto.TECH1,
                PersonnelName2    = dto.TECH2,
                RepairName        = dto.DESCR,
            }).ToList();

            // // find fm order.RepairName
            // IList<RepairType> repairTypes = (await GetRepairTypes(equipment)).ToList();
            // foreach (FmOrder order in orders)
            // {
            //     string repairName = repairTypes.FirstOrDefault(a => a.Code == order.RepairCode)?.Name;
            //
            //     // in case we don't find the code name, show the code
            //     if ( null == repairName )
            //         repairName = order.RepairCode;
            //
            //     order.RepairName = repairName;
            // }

            return orders;
        }

        public async Task<IEnumerable<Equipment>> GetEquipment(string buildingNumber)
        {
            var url = BuildUrl("equipment",
                new Dictionary<string, string>
                    {{"buildingname", buildingNumber}}, useMock: false);

            var obj = await GetAsync<EquipmentDTO>(url);

            return obj.Data.Details.Equipment.Select(dto => new Equipment
            {
                Number          = dto.Number,
                Description     = dto.Description,
                PersonnelNumber = dto.PersonnelNumber,
                OfficeId        = dto.OfficeId,
                FirstName       = dto.FirstName,
                LastName        = dto.LastName,
                Attention       = dto.Attention,
            });
        }

        public async Task<FmOrderResponse> CreateFMOrder(FmOrder order)
        {
            var url = BuildUrl("fmorder", useMock: false);

            var requestDTO = new FmOrderRequestDTO
            {
                RepairCode        = order.RepairCode,
                FieldInstructions = order.FieldInstructions,
                EquipmentNumber   = order.EquipmentNumber,
                //PersonnelNumber   = order.PersonnelNumber,
                EstLaborHours     = order.EstLaborHours,
                EstMaterialCost   = order.EstMaterialCost,
                Currency          = order.Currency,
                StartDate         = order.StartDate,
                Tech1             = order.PersonnelNumber,
                Tech2             = order.PersonnelNumber2,
            };

            var obj = await PostAsync<FmOrderResponseDTO>(url, requestDTO);
            return new FmOrderResponse { OrderNumber = obj.Data.Detail.OrderNumber };
        }

        public async Task UpdateFmOrder(FmOrder order)
        {
            var url = BuildUrl("update_fmorder", useMock: false);

            var requestDTO = new UpdateOrderDTO
            {
                FMORDER = new UpdateOrderDTO_FMORDER
                {
                    OrderNumber = order.OrderNumber,
                    Date        = order.StartDate,
                    Tech1       = order.PersonnelNumber,
                    Tech2       = order.PersonnelNumber2,
                    FieldInstr  = order.FieldInstructions,
                    EstLaborHrs = decimal.TryParse(order.EstLaborHours,   out decimal d1)? d1: 0,
                    EstMatlCost = decimal.TryParse(order.EstMaterialCost, out decimal d2)? d2: 0,
                }
            };

            await PostAsync<UpdateOrderResponseDTO>(url, requestDTO);
        }

        private string BuildUrl(string command, Dictionary<string, string> queryParams = null, bool useMock = false)
        {
            var apiId = _config["FMOrderAPIConfig:apiid"];
            var client = _config["FMOrderAPIConfig:sap-client"];
            var defaultParams = new Dictionary<string, string>
            {
                {"sap-client", client},
                {"apiid", apiId},
            };
            if (useMock)
            {
                defaultParams["mock"] = "X";
            }

            var url = _config["FMOrderAPIConfig:BaseUrl"];
            url += command + "?";
            foreach (KeyValuePair<string, string> entry in defaultParams)
            {
                url += entry.Key + "=" + entry.Value + "&";
            }

            if (queryParams != null)
                foreach (KeyValuePair<string, string> entry in queryParams)
                {
                    url += entry.Key + "=" + entry.Value + "&";
                }

            return url;
        }

        private async Task<ResponseStatus> GetBasicResponseAsync<T>(string url)
        {
            try
            {
                var response = await _client.GetAsync(url);

                if (!response.IsSuccessStatusCode && (response.StatusCode.ToString() == "Unauthorized" || response.ReasonPhrase == "Unauthorized"))
                {
                    return new ResponseStatus { IsSuccess = false, StatusCode = response.StatusCode.ToString(), StatusMessage = "Invalid login" };
                }
                else if (response.IsSuccessStatusCode)
                {
                    return new ResponseStatus { IsSuccess = true, StatusCode = response.StatusCode.ToString(), StatusMessage = "Success" };
                }

                return new ResponseStatus { IsSuccess = false, StatusCode = response.StatusCode.ToString(), StatusMessage = response.ReasonPhrase };


            }
            catch (HttpRequestException e)
            {
                throw new SapConnectionException("Error connecting to SAP. " + e.Message);
            }
        }

        private async Task<SapiResponseBody<T>> GetAsync<T>(string url)
        {
            try
            {
                Debug.WriteLine("GET > " + url);

                var response = await _client.GetAsync(url);

#if DEBUG
                Debug.WriteLine("GET < " + (int) response.StatusCode + ": " + (await response.Content.ReadAsStringAsync()));
#endif

                if (!response.IsSuccessStatusCode && (response.StatusCode.ToString() == "Unauthorized" || response.ReasonPhrase == "Unauthorized"))
                {

                  //  return new ResponseStatus { IsSuccess = false, StatusCode= response.StatusCode.ToString(),StatusMessage= "Invalid login" }

                    throw new InvalidOperationException("Invalid login");
                }

                return await GetSapiResponseBody<T>(response);
            }
            catch (HttpRequestException e)
            {
                throw new SapConnectionException("Error connecting to SAP. " + e.Message);
            }
        }

        private void CheckResponseStatus(HttpResponseMessage response)
        {
   

        }

        private async Task<SapiResponseBody<T>> PostAsync<T>(string url, object postBody)
        {
            try
            {
                string json = JsonConvert.SerializeObject(postBody);

                Debug.WriteLine("> POST: " + url + "\n" + json);

                var response = await _client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
#if DEBUG
                string resp = await response.Content.ReadAsStringAsync();
                Debug.WriteLine("< POST: "+resp);
#endif
                return await GetSapiResponseBody<T>(response);
            }
            catch (HttpRequestException e)
            {
                throw new SapConnectionException("Error connecting to SAP. " + e.Message);
            }
        }

        private static async Task<SapiResponseBody<T>> GetSapiResponseBody<T>(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new SapUnauthorized();
            }

            // We want to see if we get a proper json body, sometimes SAP does not send JSON for some errors.
            var body = await response.Content.ReadAsStringAsync();

            // IN this Scenario we only got a status back no body.
            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                if (TryParseJSON<SapiStatus>(body, out var sapiResponse))
                {
                    if (sapiResponse.Status == "E")
                    {
                        //throw new SapDataException(ServiceCommon.BuildCustomSAPErrorMessage(sapiResponse, "Cannot Complete Order"));
                        throw new SapDataException("Cannot Complete Order");
                    }
                }

                if (TryParseJSON<SapiResponseBody<T>>(body, out var sapiResponse2))
                {
                    if (sapiResponse2.Status.Status == "E")
                    {
                        //throw new SapDataException(ServiceCommon.BuildCustomSAPErrorMessage(sapiResponse2.Status, "Cannot Complete Order"));
                        throw new SapDataException("Cannot Complete Order");
                    }
                }

                throw new SapDataException("Error returned from SAP. No message found.");
            }

            if (TryParseJSON<SapiResponseBody<T>>(body, out var obj))
            {
                return obj;
            }

            throw new SapDataException("Invalid data from SAP.");
        }


        private static bool TryParseJSON<T>(string json, out T obj)
        {
            try
            {
                obj = JsonConvert.DeserializeObject<T>(json);
                return true;
            }
            catch
            {
                obj = default(T);
                return false;
            }
        }
    }
}
