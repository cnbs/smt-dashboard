using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using smt_dashboard.Exceptions;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public class SapCatalogService : ICatalogService
    {
        private readonly HttpClient _client;
        private readonly IConfiguration _config;

        public SapCatalogService(IHttpContextAccessor httpContextAccessor, IConfiguration config, HttpClient client)
        {
            string authHeader = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            _config = config;
            _client = client;
            _client.DefaultRequestHeaders.Remove("Authorization");
            _client.DefaultRequestHeaders.Add("Authorization",
                authHeader);
            _client.DefaultRequestHeaders.Remove("cache-control");
            _client.DefaultRequestHeaders.Add("cache-control", "no-cache");
            _client.BaseAddress = new Uri(config["CatalogAPIConfig:BaseUrl"]);
            _client.DefaultRequestHeaders.Add("cnbssysid", config["CatalogAPIConfig:cnbssysid"]);
        }

        public async Task<IEnumerable<ShippingType>> GetShippingTypes()
        {
            var url = BuildUrl("shiptypes", useMock: false);

            var obj = await GetAsync<ShipTypeDTO>(url);

            return obj.Data.Detail.ShipTypes.Select(dto => new ShippingType
            {
                Number = dto.Code,
                Name = dto.Name,
                EmailRequired = dto.EmailRequired == "X",
            });
        }

        public async Task<IEnumerable<ShipTo>> GetShipTos()
        {
            var url = BuildUrl("shiptos");

            var obj = await GetAsync<ShipToDTO>(url);

            return obj.Data.Detail.Select(dto => new ShipTo
            {
                Number = dto.Number,
                Name = dto.Name,
                City = dto.City,
                StreetAddress = dto.Street,
            });
        }

        public async Task<IEnumerable<Building>> SearchBuilding(string name, string searchType)
        {
            string st;
            switch (searchType)
            {
                default:
                case "building": st = "I_BUILDING"; break;
                case "address":  st = "I_ADDRESS";  break;
                case "campus":   st = "I_CAMPUS";   break;
            }

            var url = BuildUrl("building",
                new Dictionary<string, string>
                    {{"BUILDINGNAME", name}, {st, "X"}});

            var obj = await GetAsync<BuildingDTO>(url);

            return obj.Data.Details.Building.Select(dto => new Building
            {
                Number = dto.Number,
                Street = dto.Street,
                City = dto.City,
                Description = dto.Description,
            });
        }

        public async Task<IEnumerable<Equipment>> GetEquipment(string buildingNumber)
        {
            var url = BuildUrl("equipment",
                new Dictionary<string, string>
                    {{"buildingname", buildingNumber}}, useMock: false);

            var obj = await GetAsync<EquipmentDTO>(url);
            
            return obj.Data.Details.Equipment.Select(dto => new Equipment
            {
                Number      = dto.Number,
                Description = dto.Description,
                Attention   = dto.Attention,
            });
        }


        public async Task<IEnumerable<OrderHistory>> GetOrderHistory(DateTime from, DateTime to)
        {
            return new List<OrderHistory>
            {
                new OrderHistory {Number = "001", Description = "Order 123", Date = DateTime.Today, Total = 123.45},
                new OrderHistory {Number = "002", Description = "Order 123", Date = DateTime.Today, Total = 503.25},
                new OrderHistory {Number = "003", Description = "Order 123", Date = DateTime.Today, Total = 1234.45},
            };
        }

        public async Task<Order> GetOrderDetails(string orderNumber)
        {
            return new Order
            {
                Number = "001",
                Equipment = new Equipment {Number = "S23497475", Description = "ELEV 05"},
                Name = "Some Description",
                ShippingType = new ShippingType {Number = "01", Name = "Standard"},
                ShipTo = new ShipTo {Number = "01", Name = "Technician 1"},
                Items = new List<OrderItem>
                {
                    new OrderItem
                    {
                        Quantity = 4, Product = new ProductDetail
                        {
                            Name = "Product 01",
                            Number = "020303",
                            UnitPrice = 14.23,
                        }
                    },
                    new OrderItem
                    {
                        Quantity = 4, Product = new ProductDetail
                        {
                            Name = "Product 01",
                            Number = "020303",
                            UnitPrice = 14.23,
                        }
                    }
                }
            };
        }

        public async Task<IDictionary<string, ProductDetail>> GetProductDetails(IEnumerable<string> details)
        {
            var request = new ProductDetailsRequestDTO
            {
                Parts = details.Select(number => new ProductDetailsRequestPartDTO {MaterialNumber = number})
            };

            var url = BuildUrl("productdetails", useMock: false);

            var obj = await PostAsync<ProductDetailDTO>(url, request);

            return obj.Data.Details.Products.ToDictionary(dto => dto.Number, dto => new ProductDetail
            {
                Number = dto.Number,
                Name = dto.Name,
                UnitPrice = dto.UnitPrice,
                Currency = dto.Currency,
            });
        }

        public async Task<UserDetail> GetUserDetail()
        {
            var url = BuildUrl("userdetail",
                new Dictionary<string, string>
                    { }, useMock: false);

            var obj = await GetAsync<UserDetailResponseDTO>(url);

            return new UserDetail
            {
                CanOrder = obj.Data.Details.CanOrder == "X", // force to true for testing.
                EmailAddress = obj.Data.Details.EmailAddress,
            };
        }

        public async Task<NewOrderResponse> SubmitOrder(NewOrder order)
        {
            var url = BuildUrl("partsorder", useMock: false);

            var requestDTO = new NewOrderRequestDTO
            {
                LineItems = order.LineItems.Select(item => new NewOrderLineItemDTO
                    {Quantity = item.Quantity, MaterialNumber = item.MaterialNumber}),
                NotifyEmail     = order.NotifyEmail,
                PriorityNumber  = order.ShippingTypeNumber,
                EquipmentNumber = order.EquipmentNumber,
                ShipToNumber    = order.ShipToNumber,

                Attention       = order.Attention,
                ChargeNumber    = order.ChargeNumber,
            };

            var obj = await PostAsync<NewOrderResponseDTO>(url, requestDTO);
            return new NewOrderResponse {OrderNumber = obj.Data.Detail.OrderNumber};
        }

        public async Task<Visit> GetCharge(string equipmentNumber, string userId)
        {
            var url = BuildUrl("chargeno", useMock: false);

            // var requestDTO = new GetChargeRequestDTO
            // {
            //     EquipmentNumber = equipmentNumber,
            //     UserId          = userId,
            // };

            var requestDTO = new GetChargeRequestDTO
            {
                Para = new List<GetChargeRequestDTO_Para>
                {
                    new GetChargeRequestDTO_Para
                    {
                        Name  = "EQUIPNO",
                        Value = equipmentNumber,
                    },
                    new GetChargeRequestDTO_Para
                    {
                        Name  = "USERID",
                        Value = userId,
                    },
                },
            };

            var obj = await PostAsync<Visit>(url, requestDTO);
            return obj.Data;
        }

        private string BuildUrl(string command, Dictionary<string, string> queryParams = null, bool useMock = false)
        {
            var apiId = _config["CatalogAPIConfig:apiid"];
            var client = _config["CatalogAPIConfig:sap-client"];
            var defaultParams = new Dictionary<string, string>
            {
                {"sap-client", client},
                {"apiid", apiId},
            };
            if (useMock)
            {
                defaultParams["mock"] = "X";
            }

            var url = _config["CatalogAPIConfig:BaseUrl"];
            url += command + "?";
            foreach (KeyValuePair<string, string> entry in defaultParams)
            {
                url += entry.Key + "=" + entry.Value + "&";
            }

            if (queryParams != null)
                foreach (KeyValuePair<string, string> entry in queryParams)
                {
                    url += entry.Key + "=" + entry.Value + "&";
                }

            return url;
        }

        private async Task<SapiResponseBody<T>> GetAsync<T>(string url)
        {
            try
            {
                var response = await _client.GetAsync(url);
                return await GetSapiResponseBody<T>(response);
            }
            catch (HttpRequestException e)
            {
                throw new SapConnectionException("Error connecting to SAP. " + e.Message);
            }
        }


        private async Task<SapiResponseBody<T>> PostAsync<T>(string url, object postBody)
        {
            try
            {
                string json = JsonConvert.SerializeObject(postBody);

                Debug.WriteLine("> POST: " + url + "\n" + json);

                var response = await _client.PostAsync(url, new StringContent(json));
#if DEBUG
                var resp = await response.Content.ReadAsStringAsync();
                Debug.WriteLine("< POST: " + resp);
#endif
                return await GetSapiResponseBody<T>(response);
            }
            catch (HttpRequestException e)
            {
                throw new SapConnectionException("Error connecting to SAP. " + e.Message);
            }
        }

        private static async Task<SapiResponseBody<T>> GetSapiResponseBody<T>(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new SapUnauthorized();
            }

            // We want to see if we get a proper json body, sometimes SAP does not send JSON for some errors.
            var body = await response.Content.ReadAsStringAsync();

            // IN this Scenario we only got a status back no body.
            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                if (TryParseJSON<SapiStatus>(body, out var sapiResponse))
                {
                    if (sapiResponse.Status == "E")
                    {
                        throw new SapDataException(ServiceCommon.BuildCustomSAPErrorMessage(sapiResponse, "Cannot Complete Order"));                       
                    }
                }

                if (TryParseJSON<SapiResponseBody<T>>(body, out var sapiResponse2))
                {
                    if (sapiResponse2.Status.Status == "E")
                    {
                        throw new SapDataException(ServiceCommon.BuildCustomSAPErrorMessage(sapiResponse2.Status, "Cannot Complete Order"));
                    }
                }

                throw new SapDataException("Error returned from SAP. No message found.");
            }

            if (TryParseJSON<SapiResponseBody<T>>(body, out var obj))
            {
                return obj;
            }

            throw new SapDataException("Invalid data from SAP.");
        }

        private static bool TryParseJSON<T>(string json, out T obj)
        {
            try
            {
                obj = JsonConvert.DeserializeObject<T>(json);
                return true;
            }
            catch
            {
                obj = default(T);
                return false;
            }
        }
    }
}
