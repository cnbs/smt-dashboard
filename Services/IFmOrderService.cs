﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public interface IFmOrderService
    {
        Task<IEnumerable<Technician>> GetTechnicians(string officeId);

        Task<IEnumerable<Technician>> GetMockTechnicians(string officeId);

        Task<IEnumerable<RepairType>> GetRepairTypes(string equipmentNumber);

        Task<IEnumerable<RepairType>> GetMockRepairTypes(string equipmentNumber);

        Task<IEnumerable<Building>> SearchBuilding(string name, string searchType);

        Task<IEnumerable<Equipment>> GetEquipment(string buildingNumber);

        Task<FmOrderResponse> CreateFMOrder(FmOrder order);

        Task UpdateFmOrder(FmOrder order);

        Task<ResponseStatus> CheckLogin();

        Task<IEnumerable<FmOrder>> SearchFmOrders(string equipment, string officeId);
    }
}