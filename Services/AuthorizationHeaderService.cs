using System;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using smt_dashboard.Interfaces;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public class AuthorizationHeaderService : IAuthorizationHeaderService
    {
        public AuthenticatedUser GetAuthorizedUserFromHeaderAuthentication(string authHeader, string supervisorIdListString)
        {
            string[] userpass = Encoding.UTF8.GetString(Base64UrlTextEncoder.Decode(authHeader.Substring("Basic ".Length))).Split(':');
            var user = new AuthenticatedUser
            {
                Username = userpass[0],
                Password = userpass[1],
                SupervisorIdListString = supervisorIdListString
            };

            return user;
        }
    }
}
