using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public interface ICatalogService
    {
        Task<IEnumerable<ShippingType>> GetShippingTypes();
        Task<IEnumerable<ShipTo>> GetShipTos();
        Task<IEnumerable<Building>> SearchBuilding(string name, string searchType);
        
        Task<IEnumerable<Equipment>> GetEquipment(string buildingNumber);
        
        Task<IEnumerable<OrderHistory>> GetOrderHistory(DateTime from, DateTime to);
        
        Task<Order> GetOrderDetails(string orderNumber);
        
        Task<IDictionary<string, ProductDetail>> GetProductDetails(IEnumerable<string> details);
        
        Task<UserDetail> GetUserDetail();
        
        Task<NewOrderResponse> SubmitOrder(NewOrder order);

        Task<Visit> GetCharge(string equipmentNumber, string userId);
    }
}