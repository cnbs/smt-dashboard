﻿using smt_dashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace smt_dashboard
{
    public static class ServiceCommon
    {
        public static string BuildBasicSAPErrorMessage(SapiStatus status)
        {
            //TODO: remember to uncomment
            //if (status.MessageLanguage.Contains("Parts Order could not be created", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    return "Not Authorized. Only Superintendents can order parts.";
            //}
            //else
            {
                return status.MessageNumber + " - " +
                status.MessageId + " - " +
                status.MessageLanguage;
            }
        }

        public static string BuildCustomSAPErrorMessage(SapiStatus status, string title)
        {
            return title + " - " + status.MessageLanguage;
        }

    }
}
