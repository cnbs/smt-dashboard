using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using smt_dashboard.Models;

namespace smt_dashboard
{
    public class MockCatalogService : ICatalogService
    {
        public async Task<IEnumerable<ShippingType>> GetShippingTypes()
        {
            return new List<ShippingType>
            {
                new ShippingType {Number = "01", Name = "Standard", EmailRequired = true},
                new ShippingType {Number = "02", Name = "Two Day", EmailRequired = false},
                new ShippingType {Number = "03", Name = "Three Week", EmailRequired = true},
            };
        }

        public async Task<IEnumerable<ShipTo>> GetShipTos()
        {
            return new List<ShipTo>
            {
                new ShipTo {Number = "S45433004502", Name = "MORGAN DUNKAN SCHINDLER CORP", City = "WARRENTOWN", StreetAddress ="2344 JORDAN DR"},
                new ShipTo {Number = "S45433004503", Name = "MORGAN DUNKAN SCHINDLER CORP", City = "WARRENTOWN", StreetAddress ="2344 JORDAN DR"},
                new ShipTo {Number = "S45433004504", Name = "MORGAN DUNKAN SCHINDLER CORP", City = "WARRENTOWN", StreetAddress ="2344 JORDAN DR"},
                new ShipTo {Number = "S45433004505", Name = "MORGAN DUNKAN SCHINDLER CORP", City = "WARRENTOWN", StreetAddress ="2344 JORDAN DR"},
                new ShipTo {Number = "S45433004506", Name = "MORGAN DUNKAN SCHINDLER CORP", City = "WARRENTOWN", StreetAddress ="2344 JORDAN DR"},
                new ShipTo {Number = "S45433004507", Name = "MORGAN DUNKAN SCHINDLER CORP", City = "WARRENTOWN", StreetAddress ="2344 JORDAN DR"},
            };
        }


        public async Task<IEnumerable<Equipment>> GetEquipment(string buildingNumber)
        {
            return new List<Equipment>
            {
                new Equipment {Number = "123", Description = "First Elevator"}
            };
        }

        public async Task<IEnumerable<Building>> SearchBuilding(string name, string searchType)
        {
            if (!string.IsNullOrEmpty(name))
            {
                return new List<Building>
                {
                    new Building {Number = "S23497475", Description = "BUILDING 05", Street = "999 City Street", City = "Louisville"},
                };
            }

            if (!string.IsNullOrEmpty(name))
            {
                return new List<Building>
                {
                    new Building {Number = "S23497473", Description = "BUILDING 01", Street = "999 City Street"},
                    new Building {Number = "S23497474", Description = "BUILDING 02", Street = "999 City Street"},
                };
            }

            return new List<Building>
            {
                new Building {Number = "S23497473", Description = "BUILDING 04", Street = "999 City Street"},
                new Building {Number = "S23497474", Description = "BUILDING 05", Street = "999 City Street"},
                new Building {Number = "S23497475", Description = "BUILDING 06", Street = "999 City Street"},
            };
        }

        public async Task<IEnumerable<OrderHistory>> GetOrderHistory(DateTime from, DateTime to)
        {
            return new List<OrderHistory>
            {
                new OrderHistory {Number = "001", Description = "Order 123", Date = DateTime.Today, Total = 123.45},
                new OrderHistory {Number = "002", Description = "Order 123", Date = DateTime.Today, Total = 503.25},
                new OrderHistory {Number = "003", Description = "Order 123", Date = DateTime.Today, Total = 1234.45},
            };
        }

        public async Task<Order> GetOrderDetails(string orderNumber)
        {
            return new Order
            {
                Number = "001",
                Equipment = new Equipment {Number = "S23497475", Description = "ELEV 05"},
                Name = "Some Description",
                ShippingType = new ShippingType {Number = "01", Name = "Standard"},
                ShipTo = new ShipTo {Number = "01", Name = "Technician 1"},
                Items = new List<OrderItem>
                {
                    new OrderItem
                    {
                        Quantity = 4, Product = new ProductDetail
                        {
                            Name = "Product 01",
                            Number = "020303",
                            UnitPrice = 14.23,
                        }
                    },
                    new OrderItem
                    {
                        Quantity = 4, Product = new ProductDetail
                        {
                            Name = "Product 01",
                            Number = "020303",
                            UnitPrice = 14.23,
                        }
                    }
                }
            };
        }

        public async Task<IDictionary<string, ProductDetail>> GetProductDetails(IEnumerable<string> details)
        {
            return new Dictionary<string, ProductDetail>
            {
                {"MAT-1", new ProductDetail
                {
                    Name = "Product 1",
                    Number = "MAT-1",
                    UnitPrice = 100.00,
                }},
                {"MAT-2", new ProductDetail
                {
                    Name = "Product 2",
                    Number = "MAT-2",
                    UnitPrice = 100.00,
                }},
                {"MAT-3", new ProductDetail
                {
                    Name = "Product 3",
                    Number = "MAT-3",
                    UnitPrice = 100.00,
                }},
                {"MAT-4", new ProductDetail
                {
                    Name = "Product 4",
                    Number = "MAT-4",
                    UnitPrice = 100.00,
                }},
            };
        }

        public async Task<UserDetail> GetUserDetail()
        {
            return new UserDetail {  CanOrder = true, EmailAddress = "user@email.com"};

        }

        public async Task<NewOrderResponse> SubmitOrder(NewOrder order)
        {
            return new NewOrderResponse{OrderNumber = "12345123"};
        }

        public async Task<Visit> GetCharge(string equipmentNumber, string userId)
        {
            throw new NotImplementedException();
        }
    }
}
