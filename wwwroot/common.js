﻿//Remember to talk about updating other files that share these functions

export function limitToNDecimalPlaces(decimalPlaces) {

    return function (e) {
        if (decimalPlaces !== "undefined" && !isNaN(e.key)) {
            //var newValue = Number(e.target.value);
            //e.target.value = parseFloat(e.key).toFixed(decimalPlaces); //Dont use parse if need retain exact user input

            var newTextValue = e.target.value;
            var len = newTextValue.length;
            var index = newTextValue.indexOf('.');
            var adjustPlace = decimalPlaces + 1;

            if (index >= 0) {
                var CharAfterdot = len - index;
                if (CharAfterdot > adjustPlace) {
                    e.target.value = newTextValue.substring(0, newTextValue.length - 1);
                    return;
                }
            }
        }
       
    }
} 

 /* For keypress event */
export function numericWithOneDecimalOnly() {

    return function (e) {
        var keyCodeEntered = (e.which) ? e.which : (window.event.keyCode) ? window.event.keyCode : -1;

        if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {
            return true;
        }
        else if (keyCodeEntered == 46) { // '.' decimal point...  
            // Allow only 1 decimal point ('.')...  
            if ((e.target.value) && (e.target.value.indexOf('.') >= 0)) {
                e.preventDefault();
                return false;
            }
            else {
                return true;
            }
        }

        e.preventDefault();
        return false;
    }
} 

//export function handleCustomOrderErrors_OBSOLETE(json) {
//    //json = "3 - ZCNBS_C - Parts Order could not be created"
//    if (json.toLowerCase().indexOf('Parts Order could not be created'.toLowerCase()) !== -1) {
//        alert("Not Authorized. Only Superintendents can order parts.");
//        return;
//    }

//    alert(json);
//} 


function isNumberWithCurrency_obsolete(val) {
    return
        !isNaN(val.toString().replace("$", ""))
        && val !== "$"
        && val !== "";
}



//key up
function limitToNDecimalPlaces_OBSOLETE(decimalPlaces) {

    return function (e) {

        if (decimalPlaces !== "undefined" && !isNaN(e.key)) {

            var newValue = Number(e.target.value);
            var newTextValue = e.target.value;

            // alert(estLaborHoursCheck + "|" + newTextValue.length + "|" + newTextValue);

            var len = newTextValue.length;
            var index = newTextValue.indexOf('.');

            if (index >= 0) {
                var CharAfterdot = (len) - index;
                if (CharAfterdot > 2) {
                    e.target.value = newTextValue.substring(0, newTextValue.length - 1);
                    return;
                }
            }
        }
    }
}


export function validateEmail(email) {
    let re = /^\S+@\S+$/;
    return re.test(String(email).toLowerCase());
}

export function setBodyNoScroll() {
    document.body.classList.add('no-scroll');
}

export function setBodyScroll() {
    document.body.classList.remove('no-scroll');
}

//$(".extended-date-picker").on("change", function () {
//    this.setAttribute(
//        "data-date",
//        moment(this.value, "YYYY-MM-DD")
//            .format(this.getAttribute("data-date-format"))
//    )
//}).trigger("change")

export function isNullOrEmpty(value) {
    if (typeof value === "undefined") {
        return true;
    }
    if (value == null) {
        return true;
    }
    if (typeof value === 'string' || value instanceof String) {
        return value === '';
    }
    if (typeof value === 'function') {
        throw Error("functions can not be empty.");
    }
    if (Array.isArray(value)) {
        return value.length === 0;
    }
    if (typeof value === 'object') {
        return Object.keys(value).length === 0;
    }

    throw Error("Unable to determine if value is NullOrEmpty");
}

export function getValue(value, defaultValue) {
    if (typeof value === "boolean") {
        return value;
    }
    if (isNullOrEmpty(value)) {
        return defaultValue;
    }
    return value;
}

export function isInt(n) {
    return n % 1 === 0;
}

export function isValidDate(date) {
    typeof date.getMonth === 'function';
}