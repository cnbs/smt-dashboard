import {html} from 'https://unpkg.com/lit-html?module';

function goToOrder(controller, orderNumber) {
  return function (e) {
    controller.goToOrder(orderNumber);
  };
}
export const orderHistory = (controller) => html`
  <div>
    <div class="row justify-content-space-between align-items-center order-history-header">
        <button class="btn btn-flat btn-lg"> < Catalog</button>
        <h1>Order History</h1>
        <div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-space-between">
        <div><span>Order Date</span>
        <input 
            type="date" 
            class="order-date-picker"
        /> to <input type="date" class="order-date-picker"/></div>
        <div>${controller.state.orderHistory.length} Orders</div>
    </div>
    ${controller.state.orderHistory.map((order) => html`
        <div class="card mt-4 row justify-content-space-between" @click="${goToOrder(controller, order.Number)}">
            <div class="col justify-content-center">
                #${order.Number}
            </div>
            <div class="col justify-content-center">
               ${order.Date} 
            </div>
            <div class="col justify-content-center flex">
                ${order.Description}
            </div>
            <div class="col justify-content-center">
                ${order.Total}
            </div>
        </div>
    `)}
</div>
`;
