﻿import { html, render as _render } from 'https://unpkg.com/lit-html?module';
import * as common from './common.js';

/** FM Order **/

/**
 * @external invokeCSharpAction
 */


function createFMOrder(controller) {
    return function (e) {
        common.setBodyNoScroll();
        controller.createFmOrder();
    };
}

function loginClick(controller) {
    return function (e) {
        e.preventDefault();
        controller.login(document.getElementById("username").value, document.getElementById("password").value)

        var isvalid = controller.checkLogin();

        if (!isvalid) {

            controller.state.currentErrorMessage = "Invalid Login!";
            controller.state.showOrderBasicErrorsModal = true;
            controller.render();

            return;
        }

        controller.handleActionPayload({
            "action": "create-fmorder",
            "payload": {
                "username": document.getElementById("username").value,
                "password": document.getElementById("password").value
            }
        });
    };
}

function updateSearchBuildingsText(controller) {
    return function (e) {
        controller.state.buildingSearchString = e.target.value;
    }
}

function updateSearchType(controller) {
    return function (e) {
        controller.state.searchType = e.target.value;
    }
}

function searchBuildings(controller) {
    // let timerId;
    return function (e) {
        //   if (timerId) {
        //     clearTimeout(timerId);
        //   }
        // controller.state.buildingSearchString = e.target.value;
        // timerId = setTimeout(() => {
        controller.searchBuildings();
        // timerId = null;
        // }, 1000);
    };
}

function toggleEquipment(controller, building) {
    return function (e) {
        building._opened = !building._opened;
        if (building._opened) {
            controller.loadEquipmentForBuilding(building.Number);
        }
        controller.render();
    };
}


function onNotifyStartDateChange(controller) {
    return function (e) {

        controller.state.fmOrder.startDate = e.target.value;

        //if (controller.isValidStartDate(e.target.value)) {
        //    controller.state.fmOrder.startDate = e.target.value;
        //} else {
        //    e.target.value = null;
        //}

    };
}

function onNotifyEstLaborHoursChange(controller) {
    return function (e) {
        //Make sure not bad number
        e.target.value = Number(e.target.value);
        controller.state.fmOrder.estLaborHours = e.target.value;
    };
}

function onNotifyEstMaterialCostChange(controller) {
    return function (e) {
        //Make sure not bad number
        e.target.value = Number(e.target.value);
        controller.state.fmOrder.estMaterialCost = e.target.value;
    };
}

function onNotifyFieldInstructionsChange(controller) {
    return function (e) {
        controller.state.fmOrder.fieldInstructions = e.target.value;
    };
}

function formatDollar(value) {
    return parseFloat(Math.round(value * 100) / 100).toFixed(2);
}

function openModal(controller) {
    return function (e) {
        common.setBodyNoScroll();
        controller.state.showEquipmentModal = true;
        controller.render();
    };
}


function openTechniciansModal(controller, tech) {
   
    return function (e) {
        common.setBodyNoScroll();
        //controller.state.showTechniciansModal = true;
        //controller.render();

        controller.state.selectTech = tech;

        controller.searchTechnicians();
    };
}

function openRepairTypesModal(controller) {
    return function (e) {

        if (controller.state.fmOrder.orderNumber) { } else {
            common.setBodyNoScroll();
       
            controller.searchRepairTypes();
        }
    };
}

function hideModal(controller, force = false) {
    return function (e) {
        if (!force && e.target !== this)
            return;
        common.setBodyScroll();
        controller.state.showEquipmentModal = false;
        controller.state.showOrderErrorsModal = false;
        controller.state.showOrderBasicErrorsModal = false;
        controller.state.currentErrorMessage = "";

        controller.state.showRepairTypesSelectModal = false;
        controller.state.showTechniciansModal = false;
        
        controller.render();
    };
}

function openSubmitOrderModal(controller) {
    return function (e) {
        controller.state.submitOrderModal = true;
        controller.render();
    };
}


function hideSubmitOrderModal(controller, force = false) {
    return function (e) {
        if (!force && e.target !== this)
            return;
        controller.state.submitOrderModal = false;
        controller.render();
    };
}

function selectEquipment(controller, building, equipment) {
    return function (e) {
        controller.state.showEquipmentModal = false;
        controller.state.fmOrder.equipment = { ...equipment };
        controller.state.fmOrder.building = { ...building };

        if (equipment != null && !common.isNullOrEmpty(equipment.FirstName)) {
            controller.state.technicianSelected = controller.state.fmOrder.technician = { PersonnelNumber: equipment.PersonnelNumber, TechFullName: `${equipment.FirstName} ${equipment.LastName}` };
            controller.state.technicianSelected2 = null;
        }

        delete controller.state.fmOrder.equipment._opened;
        common.setBodyScroll();
        //controller.validateOrder();

        controller.searchFmorders();

        controller.render();
    };
}

function selectRepairType(controller, repairType) {
    return function (e) {
        controller.state.showRepairTypesSelectModal = false;
        controller.state.fmOrder.repairType = { ...repairType };
        common.setBodyScroll();
        //controller.validateOrder();
        controller.render();
    };
}

function returnToFmOrderAfterOrderSubmit(controller) {
    return function (e) {

       // location.href = location.href;  
        //location.reload();
        controller.resetFmOrder();
    };
}


function selectTechnician(controller, technician) {
    
    return function (e) {
        controller.state.showTechniciansModal = false;

        if (controller.state.selectTech === 1) {
            controller.state.technicianSelected = controller.state.fmOrder.technician = { ...technician };
        } else {
            controller.state.technicianSelected2 = controller.state.fmOrder.technician2 = { ...technician };
        }
       //controller.state.selectTechnician = { ...technician };

        common.setBodyScroll();
        //controller.validateOrder();
        controller.render();
    };
}

function selectFmorder(controller, fmorder) {
    
    return function (e) {
        controller.state.showFmordersModal = false;

        if (fmorder) {
            controller.state.fmOrder.orderNumber       = fmorder.OrderNumber;   
            controller.state.fmOrder.estMaterialCost   = fmorder.EstMaterialCost;
            controller.state.fmOrder.estLaborHours     = fmorder.EstLaborHours;
            controller.state.fmOrder.fieldInstructions = fmorder.FieldInstructions;
            //controller.state.fmOrder.startDate         = fmorder.StartDate;
            controller.state.fmOrder.startDate         = moment().format('YYYY-MM-DD');

            controller.state.technicianSelected  = controller.state.fmOrder.technician  = {PersonnelNumber: fmorder.PersonnelNumber,  TechFullName: fmorder.PersonnelName};
            controller.state.technicianSelected2 = controller.state.fmOrder.technician2 = {PersonnelNumber: fmorder.PersonnelNumber2, TechFullName: fmorder.PersonnelName2};

            controller.state.fmOrder.repairType = {Code: fmorder.RepairCode, Name: fmorder.RepairName}
        }
        else {
            controller.state.fmOrder.orderNumber       = null;   
            controller.state.fmOrder.estMaterialCost   = "";
            controller.state.fmOrder.estLaborHours     = "";
            controller.state.fmOrder.fieldInstructions = "";
            controller.state.fmOrder.startDate         = moment().format('YYYY-MM-DD');

            controller.state.technicianSelected  = controller.state.fmOrder.technician  = null;
            controller.state.technicianSelected2 = controller.state.fmOrder.technician2 = null;

            controller.state.fmOrder.repairType = null;
        }

        common.setBodyScroll();
        controller.render();
    };
}


function updateQuantity(controller, item) {
    return function (e) {
        let previous = item.Quantity;
        if (common.isNullOrEmpty(e.target.value) || Number.isNaN(e.target.value) || parseInt(e.target.value) < 1 || !common.isInt(e.target.value)) {
            e.target.value = previous;
            controller.render();
        } else {
            item.Quantity = parseInt(e.target.value);
            controller.updateCartState();
            controller.render();
        }
    };
}

const loadingModal = (controller) => html`

<div class="modal-container col no-gutter justify-content-center align-items-center">
    <div class="modal col loading-modal">
        <div class="flex col justify-content-center">
            <div class="row align-items-center justify-content-center">
              <div id="progressSpinner" class="spinner text-align-center">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                    <div class="bar4"></div>
                    <div class="bar5"></div>
                    <div class="bar6"></div>
                    <div class="bar7"></div>
                    <div class="bar8"></div>
                    <div class="bar9"></div>
                    <div class="bar10"></div>
                    <div class="bar11"></div>
                    <div class="bar12"></div>
              </div>
          </div>
          <div class="row align-items-center justify-content-center">
                  <p>
                    ${controller.state.loadingModalMessage}
                  </p>
          </div>
        </div>
    </div>
</div>`;

const submitOrderModal = (controller) => html`
<div class="modal-container col no-gutter justify-content-center align-items-center">
    <div class="modal col height-200px">
        <div class="flex col justify-content-center">
          <div class="flex"></div>
          <div class="flex row justify-content-space-between align-items-center">
              <div class="flex row align-items-center justify-content-end">
                    <i class="far fa-check-circle success-check-icon"></i>
              </div>
              <div class="row align-items-center justify-content-center">
                  <p>
                    Order ${controller.state.newOrder.OrderNumber} was created!
                  </p>
              </div>
              <div class="flex">
              </div>
          </div>
          <div class="flex row justify-content-center align-items-center mt-4">
              <button @click="${returnToFmOrderAfterOrderSubmit(controller)}" class="btn btn-flat btn-bordered">Return to Create FM Order</button>
          </div>
        </div>
    </div>
</div>`;

const updateOrderModal = (controller) => html`
<div class="modal-container col no-gutter justify-content-center align-items-center">
    <div class="modal col height-200px">
        <div class="flex col justify-content-center">
          <div class="flex"></div>
          <div class="flex row justify-content-space-between align-items-center">
              <div class="flex row align-items-center justify-content-end">
                    <i class="far fa-check-circle success-check-icon"></i>
              </div>
              <div class="row align-items-center justify-content-center">
                  <p>
                    Order was updated!
                  </p>
              </div>
              <div class="flex">
              </div>
          </div>
          <div class="flex row justify-content-center align-items-center mt-4">
              <button @click="${returnToFmOrderAfterOrderSubmit(controller)}" class="btn btn-flat btn-bordered">Return to Create FM Order</button>
          </div>
        </div>
    </div>
</div>`;

const fmOrderErrorsModal = (controller) => html`
<div class="modal-container col no-gutter justify-content-center align-items-center" @click="${hideModal(controller)}">
    <div class="modal col height-200px">
        <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        <div class="flex col justify-content-center">
          <div class="flex row justify-content-space-between align-items-center">
              <div class="flex row align-items-center justify-content-end">
                    <i class="fas fa-exclamation-circle error-icon"></i>
              </div>
              <div class="col justify-content-center">
                ${controller.fmOrderErrors().map(e => (
        html`<div><p>${e}</p></div>`
    ))}
              </div>
              <div class="flex">
              </div>
          </div>
        </div>
    </div>
</div>`;

const fmOrderBasicErrorsModal = (controller) => html`
<div class="modal-container col no-gutter justify-content-center align-items-center" @click="${hideModal(controller)}">
    <div class="modal col height-200px">
        <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        <div class="flex col justify-content-center">
          <div class="flex row justify-content-space-between align-items-center">
              <div class="flex row align-items-center justify-content-end">
                    <i class="fas fa-exclamation-circle error-icon"></i>
              </div>
              <div class="col justify-content-center">
               
                <div><p>${controller.state.currentErrorMessage}</p></div>
   
              </div>
              <div class="flex">
              </div>
          </div>
        </div>
    </div>
</div>`;


const equipmentSelect = (controller) => html`
<div class="modal-container" @click="${hideModal(controller)}">
    <div class="modal col">
        <div class="row justify-content-center">
            <h2>Select Equipment</h2> 
            <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        </div>
        <div class="row align-items-center mt-4">
            <div class="row align-items-start">
                <div class="col-4" style="padding-top:15px">
                    <input @keyup="${updateSearchBuildingsText(controller)}" type="text"/>
                    <div class="text-x-small lighter-text">Enter at least 2 characters to search.</div>
                </div>
                <div class="col-2" style="padding-top:15px">
                    <button @click="${searchBuildings(controller)}" class="btn btn-primary ml-4px">Search</button>
                </div>
                <div class="col-6" style="padding-left:40px">
                    <input type="radio" name="search" id="building" value="building" @click="${updateSearchType(controller)}">
                        <label for="building" style="white-space:nowrap">By Building Name</label>
                    </input><br>
                    <input type="radio" name="search" id="address" value="address" @click="${updateSearchType(controller)}">
                        <label for="address" style="white-space:nowrap">By Address</label>
                    </input><br>
                    <input type="radio" name="search" id="campus" value="campus" @click="${updateSearchType(controller)}">
                        <label for="campus" style="white-space:nowrap">By Campus</label>
                    </input>
                </div>
            </div>
        </div>
        <div class="equipment-list-container flex">
            ${controller.state.buildings.map((building) => {
        let opened = false;
        if (typeof building._opened !== "undefined") {
            opened = building._opened;
        }
        return html`
              <div class="equipment-list-building row" @click="${toggleEquipment(controller, building)}">
                  <div class="row align-items-center">
                      <div class="select-button" role="button" aria-label="Select Equipment">${opened ?
                html`<img src="${document.baseURI}wwwroot/images/icon_minus.png" srcset="${document.baseURI}wwwroot/images/icon_minus_2x.png 2x" alt="collapse building"/>` : html`<img src="${document.baseURI}wwwroot/images/icon_plus.png" srcset="${document.baseURI}wwwroot/images/icon_plus_2x.png 2x" alt="expand building"/>`}</div> 
                  </div>
                  <div class="row flex equipment-building-text align-items-center">
                    <div>${building.Description} - ${building.Street}, ${building.City}</div> 
                  </div>
              </div>
              ${controller.hasEquipment(building) && opened ? controller.getEquipment(building.Number).map(equipment => html`
                <div class="equipment-list-item row" @click="${selectEquipment(controller, building, equipment)}">
                    <div class="row flex equipment-building-list-item-text">
                      <div>${equipment.Number} - ${equipment.Description} </div> 
                    </div>
                </div>
              </div>`) : html`<div></div>`}
            `
    })}
        </div>
    </div> 
</div>
`;

const repairTypeSelectModal = (controller) => html`
<div class="modal-container" @click="${hideModal(controller)}">
    <div class="modal col shipping-modal">
        <div class="row justify-content-center">
            <h2>Select Repair Type</h2> 
            <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        </div>
        <div class="equipment-list-container mt-32px">
            ${controller.state.repairTypes.map((repairType) => {
        return html`
              <div class="equipment-list-building row" @click="${selectRepairType(controller, repairType)}">
                  <div class="col flex">
                    <div>${repairType.Code} - ${repairType.Name}</div> 
                  </div>
              </div>`
    })}
        </div>
    </div> 
</div>
`;


const technicianSelectModal = (controller) => html`
<div class="modal-container" @click="${hideModal(controller)}">
    <div class="modal col shipping-modal">
        <div class="row justify-content-center">
            <h2>Select Technician</h2> 
            <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        </div>
        <div class="equipment-list-container mt-32px">
            ${controller.state.technicians.map((technician) => {
        return html`
              <div class="equipment-list-building row" @click="${selectTechnician(controller, technician)}">
                  <div class="col flex">
                    <div>${technician.TechFullName}</div> 
                  </div>
              </div>`
    })}
        </div>
    </div> 
</div>
`;

const fmorderSelectModal = (controller) => html`
<div class="modal-container" @click="${hideModal(controller)}">
    <div class="modal col shipping-modal" style="width: 800px; height: 800px">
        <div class="row justify-content-center">
            <h2>Select Order</h2> 
        </div>
        <div class="equipment-list-container mt-32px">
            <div class="equipment-list-building row">
                  <div class="col" style="width: 100px">
                    <div>Visit / Repair</div>
                  </div>
                  <div class="col" style="width: 500px">
                    <div>Work Description</div>
                  </div>
                  <div class="col" style="width: 100px">
                    <div>Est. Hrs</div>
                  </div>
            </div>
            ${controller.state.fmorders.map((fmorder) => {
    return html`
            <div class="equipment-list-building row"  @click="${selectFmorder(controller, fmorder)}">
                <div class="col" style="width: 100px">
                  <div>${fmorder.OrderNumber}</div>
                </div>
                <div class="col" style="width: 500px">
                  <div>${fmorder.Description}</div>
                </div>
                <div class="col" style="width: 100px">
                  <div>${fmorder.EstLaborHours}</div>
                </div>
            </div>`;
    })}
        </div>

        <div class="flex col align-items-center justify-content-center" style="margin-top:20px">
            <button class="btn btn-primary btn-lg" @click="${selectFmorder(controller, null)}">Create FM Order</button>
        </div>
    </div> 
</div>
`;

const techniciansDropdown = (controller) => html`
    <div @click="${openTechniciansModal(controller,1)}" class="fake-select header-details-select">
        ${controller.state.technicianSelected ? html`<div>${controller.state.technicianSelected.TechFullName}</div>` : html`Select Technician`}
    </div>
`;

const techniciansDropdown2 = (controller) => html`
    <div @click="${openTechniciansModal(controller,2)}" class="fake-select header-details-select">
        ${controller.state.technicianSelected2 ? html`<div>${controller.state.technicianSelected2.TechFullName}</div>` : html`Select Technician`}
    </div>
`;

const repairTypesDropdown = (controller) => html`
    <div @click="${openRepairTypesModal(controller)}" class="fake-select header-details-select">
        ${controller.state.fmOrder.repairType ? html`<div>${controller.state.fmOrder.repairType.Name}</div>` : html`Select Repair Type`}
    </div>
`;


function lineTotal(i) {
    return i.Quantity * i.Product.UnitPrice;
}

function invoiceTotal(items) {
    return items.reduce((current, item) => {
        return lineTotal(item) + current
    }, 0);
}




const manualLogin = (controller) => html`
  <div class="container">
    <form name="login-form">
        <p>SAP Server: @Model.SAPServer<br />App ID: @Model.AppId</h2></p>
        <div>
            <label>Username</label> 
            <input type="text" value="" id="username" placeholder="Username"/>
        </div>
        <div>
            <label>Password</label> 
            <input type="password" value="" id="password" placeholder="Password"/>
        </div>
        <button class="btn" @click="${loginClick(controller)}" type="submit">Login</button>
    </form>
  </div>
`;


const fmOrderHeader = (controller) => html`<div>
    <div class="row justify-content-space-between align-items-center order-header">
        <div class="flex col align-items-center justify-content-center">
            <h1>Select equipment to proceed</h1>
        </div>
    </div>
</div>`;


const fmOrderDetail = (controller) => {
    return html`
<div class="container">

    <div class="order-details card row">
        <div class="col flex-1 mr-40px">
            <div class="row justify-content-space-between align-items-center equipment-select-container">
                <div class="lighter-text col no-gutter-left text-small">Equipment</div>
                <div @click="${openModal(controller)}"><img src="${document.baseURI}wwwroot/images/icon_magnify.png" srcset="${document.baseURI}wwwroot/images/icon_magnify_2x.png 2x"/></div>
            </div>
            <div class="row mt-18px ml-20px">
                ${(controller.state.fmOrder.equipment == null || controller.state.fmOrder.building == null) ? html`
                  <div></div>
                ` : html`
                <div class="flex">
                    <div>${controller.state.fmOrder.building.Number}</div>
                    <div>${controller.state.fmOrder.building.Description}</div>
                    <div>${controller.state.fmOrder.building.Street}, ${controller.state.fmOrder.building.City}</div>
                    <div>${controller.state.fmOrder.equipment.Description} - ${controller.state.fmOrder.equipment.Number}</div>
                </div>`}
            </div>
            <div class="row mt-18px ml-20px">
                <div class="flex">
                    <span class="lighter-text text-small">Order Number: </span>
                    <span>${controller.state.fmOrder.orderNumber? controller.state.fmOrder.orderNumber: "(NEW)"}</span>
                </div>
            </div>
        </div>
        <div class="col d-flex flex-column flex-1">
            <div class="row header-details justify-content-space-between">
                <div class="col no-gutter justify-content-center header-details-label">
                    <span class="lighter-text text-small">Repair Type</span>
                </div>
                <div class="flex">
                    ${controller.state.fmOrder.orderNumber
                    ? html`
                      <div class="fake-select-container-readonly">
                            ${repairTypesDropdown(controller)}
                      </div>`
                    : html`
                      <div class="fake-select-container">
                            ${repairTypesDropdown(controller)}
                      </div>`
                    }
                </div>
            </div>
            <div class="row header-details justify-content-space-between">
                <div class="col no-gutter justify-content-center header-details-label">
                    <span class="lighter-text text-small">Tech 1</span>
                </div>
                <div class="flex">
                      <div class="fake-select-container">
                            ${techniciansDropdown(controller)}
                      </div>
                </div>
            </div>
            <!-- div class="row header-details justify-content-space-between">
                <div class="col no-gutter justify-content-center header-details-label">
                    <span class="lighter-text text-small">Tech 2</span>
                </div>
                <div class="flex">
                      <div class="fake-select-container">
                            ${techniciansDropdown2(controller)}
                      </div>
                </div>
            </div -->
        </div>
    </div>

    <div class="order-details card row full-width">
       <div class="flex col flex-1">
            <div class="row header-details" >
                <div class="col no-gutter justify-content-center header-details-label">
                    <span class="lighter-text text-small">Start Date</span>
                </div>
                <div class="flex" >
                    <div>
                        <input type="date" class="extended-date-picker" value="${controller.state.fmOrder.startDate}"  @change="${onNotifyStartDateChange(controller)}"/>
                    </div>
                </div>
            </div>
        </div>

       <div class="flex col flex-1">
            <div class="row header-details" >
                <div class="col no-gutter justify-content-center header-details-label" >
                    <span class="lighter-text text-small">Est Labor Hours</span>
                </div>
                <div class="flex width-50px">
                    <div >
                        <input type="number" class="number-no-spinner" step=".1" value="${controller.state.fmOrder.estLaborHours}"  @change="${onNotifyEstLaborHoursChange(controller)}" @keypress="${common.numericWithOneDecimalOnly()}" @keyup="${common.limitToNDecimalPlaces(1)}"  />
                    </div>
                </div>
            </div>
        </div>
       <div class="flex col flex-1">
            <div class="row header-details" >
                <div class="col no-gutter justify-content-center header-details-label width-130px">
                    <span class="lighter-text text-small">Est Material Cost $</span>
                </div>
                <div class="flex width-80px">
                    <div >
                       <input type="number" class="number-no-spinner" step=".1" value="${controller.state.fmOrder.estMaterialCost}" @change="${onNotifyEstMaterialCostChange(controller)}" @keypress="${common.numericWithOneDecimalOnly()}" @keyup="${common.limitToNDecimalPlaces(2)}" />
                    </div>
                </div>


            </div>
        </div>
    </div>
   
    <div class="order-details card row">
        <div class="col d-flex flex-column flex-1">
            <div class="row header-details">
                <div class="col no-gutter justify-content-center header-details-label">
                    <span class="lighter-text text-small">Field Instructions</span>
                </div>
                <div class="flex">
                    <div>
                        <input type="text" value="${controller.state.fmOrder.fieldInstructions}" @change="${onNotifyFieldInstructionsChange(controller)}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="order-details card row" >
        <div class="flex col align-items-center justify-content-center">
            <button class="btn btn-primary btn-lg" @click="${createFMOrder(controller)}" >${controller.state.fmOrder.orderNumber? "Update FM Order":"Create FM Order"}</button>
        </div>
    </div>
   
</div>
${controller.state.showEquipmentModal ? equipmentSelect(controller) : html`<div></div>`}
${controller.state.showRepairTypesSelectModal ? repairTypeSelectModal(controller) : html`<div></div>`}
${controller.state.showTechniciansModal ? technicianSelectModal(controller) : html`<div></div>`}
${controller.state.showFmordersModal ? fmorderSelectModal(controller) : html`<div></div>`}
${controller.state.showSubmitOrderModal ? submitOrderModal(controller) : html`<div></div>`}
${controller.state.showUpdateOrderModal ? updateOrderModal(controller) : html`<div></div>`}
${controller.state.showOrderErrorsModal ? fmOrderErrorsModal(controller) : html`<div></div>`}
${controller.state.showOrderBasicErrorsModal ? fmOrderBasicErrorsModal(controller) : html`<div></div>`}
${controller.state.loadingModalMessage.length > 0 ? loadingModal(controller) : html`<div></div>`}
`;
};




//<button class="btn btn-primary btn-lg" @click="${returnToFmOrderAfterOrderSubmit(controller)}" > TEST</button >
//style="width:100px;"
//<div class="order-details card row" >
//    <div class="flex col " >
//        <div class="row header-details" >
//            <div class="col no-gutter justify-content-center header-details-label">
//                <span class="lighter-text text-small">Start Date</span>
//            </div>
//            <div class="flex" >
//                <div>
//                    <input type="date" class="extended-date-picker" value="${controller.state.fmOrder.startDate}"  @change="${onNotifyStartDateChange(controller)}"/>
//                    </div>
//            </div>


//            <div class="col no-gutter justify-content-center header-details-label" style="margin-left:45px;">
//                <span class="lighter-text text-small">Est Labor Hours</span>
//            </div>
//            <div class="flex" >
//                <div style="width:100px;">
//                    <input type="number" class="number-no-spinner" step=".1" value="${controller.state.fmOrder.estLaborHours}"  @change="${onNotifyEstLaborHoursChange(controller)}" @keypress="${common.numericWithOneDecimalOnly()}" @keyup="${common.limitToNDecimalPlaces(1)}"  />
//                    </div>
//            </div>


//            <div class="col no-gutter justify-content-center header-details-label" style="flex:1; width:130px;">
//                <span class="lighter-text text-small">Est Material Cost $</span>
//            </div>
//            <div class="flex" style="flex: 1;" >
//                <div style="width:100px;" style="width:130px;margin-right:40px;">
//                    <input type="text" value="${controller.state.fmOrder.estMaterialCost}" @change="${onNotifyEstMaterialCostChange(controller)}"/>
//                    </div>
//            </div>


//        </div>
//    </div>
//</div>


class ApplicationFmOrderController {
    
    constructor() {
        this.user = "";
        this.password = "";

        this.page = "login";

		if (window.defaultSearchType === undefined) {
			window.defaultSearchType = "building";
		}

        this.state = {
            canOrder: true,
            details: {},
            equipment: {},// Map<string, <Equipment>[]> - Building number to list of equipment.
            buildings: [],
            repairTypes: [],
            technicians: [],
            total: 0,
            showEquipmentModal: false,
            showSubmitOrderModal: false,
            showUpdateOrderModal: false,
            showRepairTypesSelectModal: false,
            showOrderBasicErrorsModal: false,
            showTechniciansModal: false,
            loadingModalMessage: "Loading...",
            buildingSearchString: "",
            searchType: window.defaultSearchType,
            buildingSearchType: "number",
            currentErrorMessage: "",
            technicianSelected: null,
            technicianSelected2: null,
            isOrderValid:false,
            fmOrder: {
                startDate: moment().format('YYYY-MM-DD'),
                repairType: "",
                equipment: null,
                building: null,
                technician: "",
                personnelNumber: "",
                estMaterialCost: 0,
                estLaborHours: 0,
                fieldInstructions: "",
                items: [],
            },
        };

    }

    beginLoading(message) {
        common.setBodyNoScroll();
        this.state.loadingModalMessage = message
        this.render();
    }

    endLoading() {
        common.setBodyScroll();
        this.state.loadingModalMessage = "";
        this.render();
    }

    login(user, password) {
        this.user = user;
        this.password = password;
    }


    getCheckoutDetails(cart) {
        this.beginLoading("Loading...");
        return fetch(document.baseURI + "api/checkout-details", {
            method: "PUT",
            body: JSON.stringify(cart), // body data type must match "Content-Type" header
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Basic " + btoa(this.user + ":" + this.password),
            }
        }).then(async response => {
            this.state.details = await response.json();
            this.state.cart.items = this.state.details.OrderItems.map(a => ({ ...a }));
            if (common.isNullOrEmpty(this.state.cart.notify)) {
                this.state.cart.notify = common.getValue(this.state.details.UserDetail.EmailAddress, "");
            }
            this.state.canOrder = this.state.details.UserDetail.CanOrder;
        }).catch(e => alert(e)).finally(() => {
            this.endLoading();
        });
    }

    //searchBuildings() {
    //    //User must enter 2 or more chars to search.
    //    if (!common.isNullOrEmpty(this.state.buildingSearchString) && this.state.buildingSearchString.length > 1) {
    //        this.beginLoading("Searching...");
    //        let url = '/api/fmorder/building-search?';
    //        //let url = '/api/building-search?';
    //        url += `name=${this.state.buildingSearchString}`;
    //        return fetch(url, {
    //            method: "GET",
    //            headers: {
    //                "Content-Type": "application/json",
    //                "Authorization": "Basic " + btoa(this.user + ":" + this.password),
    //            }
    //        }).then(async response => {
    //            alert(JSON.stringify(response.body));
    //            this.state.buildings = await response.json();
    //                this.render();
    //            }).catch(e => alert(JSON.stringify(e.body))).finally(() => {
    //            this.endLoading();
    //        });
    //    }
    //}

    checkLogin() {
        //User must enter 2 or more chars to search.
        var isvalid = false;

            this.beginLoading("Signing In...");
            let url = document.baseURI+'api/fmorder/check-login';
            //let url = '/api/building-search?';
           // url += `name=${this.state.buildingSearchString}`;
            return fetch(url, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Basic " + btoa(this.user + ":" + this.password),
                }
            }).then(async response => {
                var result = await response.json();

                if (!result.IsSuccess) {
                    isvalid = false;

                    this.state.currentErrorMessage = "Invalid Login!";
                    this.state.showOrderBasicErrorsModal = true;
                    this.render();
                   // alert(result.StatusMessage);
                }
                else {
                    isvalid = true;
                }

                //return isvalid;
                //alert(JSON.stringify(result));

            }).finally(() => {
                this.endLoading();



                //if (isvalid && callbackOnSuccess !== null && callbackOnSuccess!=='undefined') {

                //    callbackOnSuccess();
                //}

                return isvalid;
            });
        
    }

    searchBuildings() {
        //User must enter 2 or more chars to search.
        if (!common.isNullOrEmpty(this.state.buildingSearchString) && this.state.buildingSearchString.length > 1) {
            this.beginLoading("Searching...");
            let url = document.baseURI+'api/fmorder/building-search?';
            //let url = '/api/building-search?';
            url += `name=${this.state.buildingSearchString}`;
            url += `&searchType=${this.state.searchType}`;
            return fetch(url, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Basic " + btoa(this.user + ":" + this.password),
                }
            }).then(async response => {
                this.state.buildings = await response.json();
                this.render();
            }).finally(() => {
                this.endLoading();
            });
        }
    }

    searchTechnicians() {
        
        if (!common.isNullOrEmpty(this.state.fmOrder.equipment)) {

            var officeId = this.state.fmOrder.equipment.OfficeId;

           // officeId = "0310"; //0210  0310

            this.beginLoading("Loading...");
            let url = document.baseURI+'api/fmorder/technicians?';
            url += `officeId=${officeId}`;
            return fetch(url, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Basic " + btoa(this.user + ":" + this.password),
                }
            }).then(async response => {
                this.state.technicians = await response.json();

                common.setBodyNoScroll();
                this.state.showTechniciansModal = true;
                this.render();

            }).finally(() => {
                this.endLoading();
            });
        }
        else {
            //alert("Must select equipment first!");
            this.state.currentErrorMessage = "Must select equipment first!";
            this.state.showOrderBasicErrorsModal = true;
            this.render();
        }
    }

    searchFmorders() {

        if (!common.isNullOrEmpty(this.state.fmOrder.equipment)) {

            var equipmentNumber = this.state.fmOrder.equipment.Number;
            var officeId = this.state.fmOrder.equipment.OfficeId;

            this.beginLoading("Loading...");
            let url = document.baseURI+'api/fmorders?';
            url += `equipment=${equipmentNumber}&${officeId}`;
            return fetch(url, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Basic " + btoa(this.user + ":" + this.password),
                }
            }).then(async response => {
                this.state.fmorders = await response.json();


                /*/ test
                this.state.fmorders = [
                    {
                        OrderNumber: "5130219795",
                        EquipmentNumber: "",
                        EstLaborHours: "10",
                        RepairCode: "3106",
                        RepairName: "Sales Rep Request",
                        Description: "Cat5 Traction CPSI/Full Load | Test",
                        EstMaterialCost: "33.22",
                        FieldInstructions: "field Instructions field Instructions field Instructions",
                        StartDate: "2021-12-01",
                        PersonnelNumber: "1001",
                        PersonnelName: "John Doe",
                        PersonnelNumber2: "1002",
                        PersonnelName2: "Jane Doe",
                    },
                    {
                        RepairCode: "TestNo1",
                        Description: "Test Work Desc1",
                        EstLaborHours: "3"
                    }
                ];
                */

                common.setBodyNoScroll();
                this.state.showFmordersModal = true;
                this.render();

            }).finally(() => {
                this.endLoading();
            });
        }
        else {
            //alert("Must select equipment first!");
            this.state.currentErrorMessage = "Must select equipment first!";
            this.state.showOrderBasicErrorsModal = true;
            this.render();
        }
    }

    searchRepairTypes() {

        if (!common.isNullOrEmpty(this.state.fmOrder.equipment)) {

            var equipmentNumber = this.state.fmOrder.equipment.Number;

            this.beginLoading("Loading...");
            let url = document.baseURI+'api/fmorder/repairtypes?';
            url += `equipmentNumber=${equipmentNumber}`;
            return fetch(url, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Basic " + btoa(this.user + ":" + this.password),
                }
            }).then(async response => {
                this.state.repairTypes = await response.json();

                common.setBodyNoScroll();
                this.state.showRepairTypesSelectModal = true;
                this.render();

            }).finally(() => {
                this.endLoading();
            });
        }
        else {
            //alert("Must select equipment first!");
            this.state.currentErrorMessage = "Must select equipment first!";
            this.state.showOrderBasicErrorsModal = true;
            this.render();
        }
    }
 

    hasEquipment(building) {
        return typeof this.state.equipment[building.Number] !== "undefined";
    }

    getEquipment(number) {
        return this.state.equipment[number];
    }

    loadEquipmentForBuilding(number) {
        this.beginLoading("Loading...");
        let url = document.baseURI+`api/fmorder/building/${number}/equipment`;
        //let url = `/api/building/${number}/equipment`;
        return fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Basic " + btoa(this.user + ":" + this.password),
            }
        }).then(async response => {
            this.state.equipment[number] = await response.json();
            this.render();
        }).catch(e => alert(e)).finally(() => {
            this.endLoading();
        });
    }

    fmOrderErrors() {
        let errors = [];

        //Equipment is required
        //Repair Type is Required
        //Technician is Required
        //Start Date xx / xx / xxxx must be today or later!
        //Est Labor Hours must be > 0.
        //Field Instructions is required

        if (common.isNullOrEmpty(this.state.fmOrder.equipment)) {
            errors.push("Equipment is required.");
        }
        if (common.isNullOrEmpty(this.state.fmOrder.repairType)) {
            errors.push("Repair Type is required.");
        }
        if (common.isNullOrEmpty(this.state.fmOrder.technician)) {
            errors.push("Technician is required.");
        }
        if (!this.isValidStartDate(this.state.fmOrder.startDate)) {
            errors.push("Start date must be today or later.")
        }
        if (this.state.fmOrder.estLaborHours == 0) {
            errors.push("Est Labor Hours must be > 0.");
        }
        if (common.isNullOrEmpty(this.state.fmOrder.fieldInstructions)) {
            errors.push("Field Instructions is required.");
        } 

        return errors;
    }

    buildFmOrder() {
        
        var fmOrder = {
            PersonnelNumber:   this.state.fmOrder.technician.PersonnelNumber,
            //PersonnelNumber2:  this.state.fmOrder.technician2.PersonnelNumber,
            RepairCode:        this.state.fmOrder.repairType.Code,
            EquipmentNumber:   this.state.fmOrder.equipment.Number,
            //StartDate:         this.state.fmOrder.startDate,
            StartDate:         moment(this.state.fmOrder.startDate).format("YYYYMMDD"),
            EstLaborHours:     this.state.fmOrder.estLaborHours,
            EstMaterialCost:   this.state.fmOrder.estMaterialCost,
            FieldInstructions: this.state.fmOrder.fieldInstructions,
            Currency:          "USD",
        }
        return fmOrder;
    }

    resetFmOrder() {
        //this.state = {
        //    canOrder: true,
        //    details: {},
        //    equipment: {},// Map<string, <Equipment>[]> - Building number to list of equipment.
        //    buildings: [],
        //    repairTypes: [],
        //    technicians: [],
        //    total: 0,
        //    showEquipmentModal: false,
        //    showSubmitOrderModal: false,
        //    showRepairTypesSelectModal: false,
        //    showTechniciansModal: false,
        //    loadingModalMessage: "", 
        //    buildingSearchString: "",
        //    buildingSearchType: "number",
        //    technicianSelected: null,
        //    isOrderValid: false,
        //    fmOrder: {
        //        startDate: moment().format('YYYY-MM-DD'),
        //        repairType: "",
        //        equipment: null,
        //        building: null,
        //        technician: "",
        //        personnelNumber: "",
        //        estMaterialCost: 0,
        //        estLaborHours: 0,
        //        fieldInstructions: "",
        //        items: [],
        //    }
        //};

        this.state.equipment= { };// Map<string, <Equipment>[]> - Building number to list of equipment.
        this.state.buildings = [];
        this.state.repairTypes = [];
        this.state.technicians = [];
        this.state.technicianSelected= null;
        this.state.technicianSelected2= null;
        this.state.showSubmitOrderModal = false;
        this.state.showUpdateOrderModal = false;

        this.state.buildingSearchString= "";
        //this.state.searchType = ""; // do not reset since the radio button stays the same
        this.state.buildingSearchType= "number";

        this.state.fmOrder.startDate= moment().format('YYYY-MM-DD');
        this.state.fmOrder.estLaborHours = 0;
        this.state.fmOrder.estMaterialCost = 0;
        this.state.fmOrder.fieldInstructions = "";
        this.state.fmOrder.building = null;
        this.state.fmOrder.equipment = null;
        this.state.fmOrder.repairType = "";
        this.state.fmOrder.personnelNumber = "";
        this.state.fmOrder.technician = "";
        this.state.fmOrder.technician2 = "";
        this.state.fmOrder.orderNumber = null;
       // this.render();

        this.page = "fmorder";

        if (typeof invokeCSharpAction === "undefined") {
            //console.log(this.state);
            this.page = "fmorder-login";
            this.render();
        } else {
            invokeCSharpAction({
                action: "reset-fmorder",
                payload: null,
            });
        }

      //  this.render();
    }

    updateFmOrder() {
        let errors = this.fmOrderErrors();
        if (errors.length === 0) {

            // TODO:
            var fmOrder = {
                RepairCode:        this.state.fmOrder.repairType.Code,
                OrderNumber:       this.state.fmOrder.orderNumber,
                PersonnelNumber:   this.state.fmOrder.technician.PersonnelNumber,
                PersonnelNumber2:  this.state.fmOrder.technician2.PersonnelNumber,
                StartDate:         moment(this.state.fmOrder.startDate).format("YYYYMMDD"),
                EstLaborHours:     this.state.fmOrder.estLaborHours,
                EstMaterialCost:   this.state.fmOrder.estMaterialCost,
                FieldInstructions: this.state.fmOrder.fieldInstructions,
            }

            let url = document.baseURI + 'api/update-fm-order';
            this.beginLoading("Updating Order...");
            return fetch(url, {
                method: "POST",
                body: JSON.stringify(fmOrder), // body data type must match "Content-Type" header
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Basic " + btoa(this.user + ":" + this.password),
                }
            }).then(async response => {
                if (response.status !== 200) {
                    let json = await response.json();
                    alert(json.error);
                } else {
                    //this.state.newOrder = await response.json();
                    this.state.showUpdateOrderModal = true;
                    //this.updateCartState(true);
                    this.render();
                }
            }).catch(e => alert(JSON.stringify(e.body))).finally(() => { 
                this.endLoading();
            });
        } else {
            this.state.showOrderErrorsModal = true;
            this.render();
            common.setBodyScroll();
        }
    }

    createFmOrder() {
        if (this.state.fmOrder.orderNumber) {
            this.updateFmOrder();
        }
        else {
            let errors = this.fmOrderErrors();
            if (errors.length === 0) {

                let url = document.baseURI + 'api/create-fm-order';
                this.beginLoading("Creating Order...");
                return fetch(url, {
                    method: "POST",
                    body: JSON.stringify(this.buildFmOrder()), // body data type must match "Content-Type" header
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Basic " + btoa(this.user + ":" + this.password),
                    }
                }).then(async response => {
                    if (response.status !== 200) {
                        let json = await response.json();
                        alert(json.error);
                    } else {
                        this.state.newOrder = await response.json();
                        this.state.showSubmitOrderModal = true;
                        //this.updateCartState(true);
                        this.render();
                    }
                }).catch(e => alert(JSON.stringify(e.body))).finally(() => { 
                    this.endLoading();
                });
            } else {
                this.state.showOrderErrorsModal = true;
                this.render();
                common.setBodyScroll();
            }
        }
    }

    init() {
        window._cnbs_sendAppMessage = this.handleActionPayload.bind(this);

        //if (typeof page === "undefined") {
        //    if (typeof invokeCSharpAction === "undefined") {
        //        this.page = "login";
        //        this.render();
        //    } else {
        //        invokeCSharpAction({
        //            "action": "checkout-ready",
        //            "payload": null
        //        });
        //    }
        //}
        //else {

            if (typeof invokeCSharpAction === "undefined") {
                this.page = "fmorder-login";
                this.render();
            } else {
                invokeCSharpAction({
                    "action": "fmorder-ready",
                    "payload": null
                });
            }
        //}
    }

    //TODO: remove this
    validateOrder() {

        this.state.isOrderValid =
            !common.isNullOrEmpty(this.state.fmOrder.equipment)
            &&
            !common.isNullOrEmpty(this.state.fmOrder.technician)
            &&
            !common.isNullOrEmpty(this.state.fmOrder.repairType)
            &&
            !common.isNullOrEmpty(this.state.fmOrder.fieldInstructions)
            &&
            this.state.fmOrder.estLaborHours > 0; //TODO: est Labor hours can only have 1 decimal place
    }

    isValidStartDate(startDate) {

        if (startDate == null) {
            return false;
        } else if (startDate == "") {
            return false;
        }

        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        var date = moment(startDate);

        //var mtoday = moment(today)

        if (date < today) {
            //this.state.currentErrorMessage = `Start date ${moment(date).format('MM/DD/YYYY')} must be today or later!`;
            //this.state.showOrderBasicErrorsModal = true;
            //this.render();
            return false;
        }

        return true;
    }

    isValidStartDate_OnDemand(startDate) {

        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        var date = moment(startDate);

        if (date < today) {
            this.state.currentErrorMessage = `Start date ${moment(date).format('MM/DD/YYYY')} must be today or later!`;
            this.state.showOrderBasicErrorsModal = true;
            this.render();
            return false;
        }
        
        return true;
    }

    render() {
        console.log(this.state);


        if (this.page === "fmorder-login") {
            _render(manualLogin(this), document.body);
        }
        if (this.page === "fmorder") {
            _render([fmOrderHeader(this), fmOrderDetail(this)], document.body);

            var isChecked = false;
            var rb1 = document.getElementById("building"); 
            var rb2 = document.getElementById("address"); 
            var rb3 = document.getElementById("campus"); 
            if (rb1 && rb2 && rb3) {
                isChecked = rb1.checked || rb2.checked || rb3.checked;
            }

            if (!isChecked) {
                var rb = document.getElementById(window.defaultSearchType);
                if ( rb ) {
                    rb.checked = true;
                } else {
                    rb = document.getElementById("building");
                    if ( rb ) rb.checked = true;
                }
            }
        }
    }

    handleActionPayload(actionPayload) {

        if (actionPayload.action === "create-fmorder") {
            this.handleFmOrderAction(actionPayload.payload);
        }
    }
    
    handleFmOrderAction(payload) {
        this.page = "fmorder";

        // Call login to set login values.
        this.login(payload.username, payload.password);

        var isvalid = this.checkLogin();

        //if (!isvalid) {
        //    this.state.currentErrorMessage = "Invalid Login!";
        //    this.state.showOrderBasicErrorsModal = true;
        //    this.render();
        //    return;
        //}


        //this.state.loadingModalMessage = '';

        //this.render();
    }

}

export const app = new ApplicationFmOrderController();

export function getApp() {
    return app;
}
