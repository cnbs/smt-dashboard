import {html, render as _render} from 'https://unpkg.com/lit-html?module';
import {orderHistory} from './order-history.js';
import {orderDetails} from './order-details.js';
import * as common from './common.js';

/**
 * @external invokeCSharpAction
 */
function validateEmail(email) {
  let re = /^\S+@\S+$/;
  return re.test(String(email).toLowerCase());
}

function setBodyNoScroll() {
  document.body.classList.add('no-scroll');
}

function setBodyScroll() {
  document.body.classList.remove('no-scroll');
}

function isNullOrEmpty(value) {
  if (typeof value === "undefined") {
    return true;
  }
  if (value == null) {
    return true;
  }
  if (typeof value === 'string' || value instanceof String) {
    return value === '';
  }
  if (typeof value === 'function') {
    throw Error("functions can not be empty.");
  }
  if (Array.isArray(value)) {
    return value.length === 0;
  }
  if (typeof value === 'object') {
    return Object.keys(value).length === 0;
  }

  throw Error("Unable to determine if value is NullOrEmpty");
}

function getValue(value, defaultValue) {
  if (typeof value === "boolean") {
    return value;
  }
  if (isNullOrEmpty(value)) {
    return defaultValue;
  }
  return value;
}

function updateSearchBuildingsText(controller) {
  return function (e) {
    controller.state.buildingSearchString = e.target.value;
  }
}

function updateSearchType(controller) {
  return function (e) {
    controller.state.searchType = e.target.value;
  }
}

function searchBuildings(controller) {
  // let timerId;
  return function (e) {
  //   if (timerId) {
  //     clearTimeout(timerId);
  //   }
    // controller.state.buildingSearchString = e.target.value;
    // timerId = setTimeout(() => {
      controller.searchBuildings();
      // timerId = null;
    // }, 1000);
  };
}

function returnToCatalogAfterOrderSubmit(controller) {
  return function (e) {
    controller.updateCartState(true);
    window.location = controller.state.catalogUrl;
  };
}

function updateShipToSearch(controller) {
  let timerId;
  return function (e) {
    if (timerId) {
      clearTimeout(timerId);
    }
    controller.state.shipToSearch = e.target.value;
    timerId = setTimeout(() => {
      controller.render();
      timerId = null;
    }, 200);
  };
}

function toggleEquipment(controller, building) {
  return function (e) {
    building._opened = !building._opened;
    if (building._opened) {
      controller.loadEquipmentForBuilding(building.Number);
    }
    controller.render();
  };
}

function submitOrder(controller) {
  return function (e) {
    setBodyNoScroll();
    controller.submitOrder();
  };
}

function loginClick(controller) {
  return function (e) {
    e.preventDefault();
    controller.login(document.getElementById("username").value, document.getElementById("password").value)
    controller.handleActionPayload({
      "action": "checkout",
      "payload": {
        "username": document.getElementById("username").value,
        "password": document.getElementById("password").value,
        "cart": {
          "header": {
            "shipTo": null,
            "shippingType": "",
            "notify": "",
            "catalogUrl": window.catalogUrl,
          },
          "lineItems": {
            'MAT-1': 1,
          },
        }
      }
    });
  };
}

function onNotifyEmailChange(controller) {
  return function (e) {
    controller.state.cart.notify = e.target.value;
    controller.updateCartState();
  };
}

function onAttentionChange(controller) {
    return function (e) {
        controller.state.cart.attention = e.target.value;
        controller.updateCartState();
    };
}

function formatDollar(value) {
  return parseFloat(Math.round(value * 100) / 100).toFixed(2);
}

function openModal(controller) {
  return function (e) {
    setBodyNoScroll();
    controller.state.showEquipmentModal = true;
    controller.render();
  };
}

function openSelectShipToModal(controller) {
  return function (e) {
    setBodyNoScroll();
    controller.state.showShipToSelectModal = true;
    controller.render();
  };
}

function openShippingTypeModal(controller) {
  return function (e) {
    setBodyNoScroll();
    controller.state.showShippingTypeModal = true;
    controller.render();
  };
}

function openChargeModal(controller) {
    return function (e) {
        setBodyNoScroll();

        controller.loadCharge(controller.state.cart.equipment).then(a => {
            controller.state.showChargeModal = true;
            controller.render();
        });
    };
}

function hideModal(controller, force = false) {
  return function (e) {
    if (!force && e.target !== this)
      return;
    setBodyScroll();
    controller.state.showEquipmentModal = false;
    controller.state.showOrderErrorsModal = false;
    controller.state.showShipToSelectModal = false;
    controller.state.showShippingTypeModal = false;
    controller.state.showOrderWarningsModal = false;
    controller.state.showChargeModal = false;
    controller.state.shipToSearch = "";
    controller.render();
  };
}

function openSubmitOrderModal(controller) {
  return function (e) {
    controller.state.submitOrderModal = true;
    controller.render();
  };
}


function hideSubmitOrderModal(controller, force = false) {
  return function (e) {
    if (!force && e.target !== this)
      return;
    controller.state.submitOrderModal = false;
    controller.render();
  };
}

function removeItem(controller, index) {
  return function (e) {
    controller.state.cart.items.splice(index, 1);
    controller.updateCartState();
    controller.render();
  };
}

function selectEquipment(controller, building, equipment) {
  return function (e) {
    controller.state.showEquipmentModal = false;
    controller.state.cart.equipment = {...equipment};
    controller.state.cart.building = {...building};
    delete controller.state.cart.equipment._opened;

    controller.state.cart.attention = equipment.Attention;

    //controller.loadCharge(controller.state.cart.equipment); 

    setBodyScroll();
    controller.updateCartState();
    controller.render();
  };
}

function selectShipTo(controller, shipTo) {
  return function (e) {
    controller.state.showShipToSelectModal = false;
    controller.state.cart.shipTo = {...shipTo};
    setBodyScroll();
    controller.updateCartState();
    controller.render();
  };
}

function selectShippingType(controller, shippingType) {
  return function (e) {
    controller.state.showShippingTypeModal = false;
    controller.state.cart.shippingType = {...shippingType};
    setBodyScroll();
    controller.updateCartState();
    controller.render();
  };
}

function selectCharge(controller, charge) {
    return function (e) {
        controller.state.showChargeModal = false;
        controller.state.cart.charge = {...charge};
        setBodyScroll();
        controller.updateCartState();
        controller.render();
    };
}

function onShippingTypeChange(controller) {
  return function (e) {
    controller.state.cart.shippingType = e.target.value;
    controller.updateCartState();
    controller.render();
  };
}

function onShipToChange(controller) {
  return function (e) {
    controller.state.cart.shipTo = e.target.value;
    controller.updateCartState();
    controller.render();
  };
}

function isInt(n) {
  return n % 1 === 0;
}

function updateQuantity(controller, item) {
  return function (e) {
    let previous = item.Quantity;
    if (isNullOrEmpty(e.target.value) || Number.isNaN(e.target.value) || parseInt(e.target.value) < 1 || !isInt(e.target.value)) {
      e.target.value = previous;
      controller.render();
    } else {
      item.Quantity = parseInt(e.target.value);
      controller.updateCartState();
      controller.render();
    }
  };
}

let orderHeader = (controller) => html`<div>
    <div class="row justify-content-space-between align-items-center order-header">
        <div class="flex row align-items-center">
            <a href="${window.catalogUrl}" class="btn btn-flat back-button lighter-text" @click="${(e) => {
  e.preventDefault();
  e.stopPropagation();
  window.history.back()
}}"> < Catalog</a>
        </div>
        <div class="flex col align-items-center justify-content-center">
            <h1>My Order</h1>
            <div><span class="text-small lighter-text">${controller.state.cart.items.length} Items</span></div>
        </div>
        <div class="flex row justify-content-end align-items-center">
            <button class="btn btn-primary btn-lg" @click="${submitOrder(controller)}" ?disabled="${controller.state.cart.items.length == 0 || !controller.state.canOrder}">Complete Order</button>
        </div>
    </div>
</div>`;

const shipToDropdown = (controller) => html`
    <div @click="${openSelectShipToModal(controller)}" class="fake-select header-details-select">
        ${controller.state.cart.shipTo ? html`<div>${controller.state.cart.shipTo.Name}</div><div class="light-text text-small">${controller.state.cart.shipTo.Number}</div>` : html`Select Ship-To`}
    </div>
`;

const shipTypesDropdown = (controller) => html`
    <div @click="${openShippingTypeModal(controller)}" class="fake-select header-details-select">
        ${controller.state.cart.shippingType ? html`<div>${controller.state.cart.shippingType.Name}</div>` : html`Select Delivery Priority`}
    </div>
`;

const chargeDropdown = (controller) => html`
    <div @click="${openChargeModal(controller)}" class="fake-select header-details-select">
        ${controller.state.cart.charge ? html`<div>${controller.state.cart.charge.EI_VISIT}</div>` : html`Select Charge`}
    </div>
`;

const manualLogin = (controller) => html`
  <div class="container">
    <form name="login-form">
        <p>SAP Server: @Model.SAPServer<br />App ID: @Model.AppId</h2></p>
        <div>
            <label>Username</label> 
            <input type="text" id="username" value="" placeholder="Username"/>
        </div>
        <div>
            <label>Password</label> 
            <input type="password" id="password" value="" placeholder="Password"/>
        </div>
        <button class="btn" @click="${loginClick(controller)}" type="submit">Login</button>
    </form>
  </div>
`;
const loadingModal = (controller) => html`

<div class="modal-container col no-gutter justify-content-center align-items-center">
    <div class="modal col loading-modal">
        <div class="flex col justify-content-space-around">
          <div id="progressSpinner" class="spinner">
              <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
                <div class="bar4"></div>
                <div class="bar5"></div>
                <div class="bar6"></div>
                <div class="bar7"></div>
                <div class="bar8"></div>
                <div class="bar9"></div>
                <div class="bar10"></div>
                <div class="bar11"></div>
                <div class="bar12"></div>
          </div>
          <div class="row align-items-center justify-content-center">
                  <p>
                    ${controller.state.loadingModalMessage}
                  </p>
          </div>
        </div>
    </div>
</div>`;

const submitOrderModal = (controller) => html`
<div class="modal-container col no-gutter justify-content-center align-items-center">
    <div class="modal col height-200px">
        <div class="flex col justify-content-center">
          <div class="flex"></div>
          <div class="flex row justify-content-space-between align-items-center">
              <div class="flex row align-items-center justify-content-end">
                    <i class="far fa-check-circle success-check-icon"></i>
              </div>
              <div class="row align-items-center justify-content-center">
                  <p>
                    Order ${controller.state.newOrder.OrderNumber} was placed!
                  </p>
              </div>
              <div class="flex">
              </div>
          </div>
          <div class="flex row justify-content-center align-items-center mt-4">
              <button @click="${returnToCatalogAfterOrderSubmit(controller)}" class="btn btn-flat btn-bordered">Return to Catalog</button>
          </div>
        </div>
    </div>
</div>`;

const orderErrorsModal = (controller) => html`
<div class="modal-container col no-gutter justify-content-center align-items-center" @click="${hideModal(controller)}">
    <div class="modal col height-200px">
        <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        <div class="flex col justify-content-center">
          <div class="flex row justify-content-space-between align-items-center">
              <div class="flex row align-items-center justify-content-end">
                    <i class="fas fa-exclamation-circle error-icon"></i>
              </div>
              <div class="col justify-content-center">
                ${controller.orderErrors().map(e => (
  html`<div><p>${e}</p></div>`
))}
              </div>
              <div class="flex">
              </div>
          </div>
        </div>
    </div>
</div>`;


const orderWarningsModal = (controller) => html`
<div class="modal-container col no-gutter justify-content-center align-items-center" @click="${hideModal(controller)}">
    <div class="modal col" style="height:200px;">
        <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        <div class="flex col justify-content-center">
          <div class="flex row justify-content-space-between align-items-center">
              <div class="flex row align-items-center justify-content-end">
                    <i class="fas fa-exclamation-circle error-icon" style="color:orange"></i>
              </div>
              <div class="col justify-content-center">
                ${controller.state.orderWarningsMessages.map(e => (
    html`<div><p>${e}</p></div>`
))}
              </div>
              <div class="flex">
              </div>
          </div>
        </div>
    </div>
</div>`;


const equipmentSelect = (controller) => html`
<div class="modal-container" @click="${hideModal(controller)}">
    <div class="modal col">
        <div class="row justify-content-center">
            <h2>Select Equipment</h2> 
            <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        </div>
        <div class="row align-items-center mt-4">
            <div class="flex">
                <div class="row">
                    <input @keyup="${updateSearchBuildingsText(controller)}" type="text" placeholder="Search for building by name"/>          
                    <button @click="${searchBuildings(controller)}" class="btn btn-primary ml-4px">Search</button>
                </div>
                <div class="text-x-small lighter-text">Enter at least 2 characters to search.</div>
            </div>

            <div class="col-6" style="padding-left:40px">
                <input type="radio" name="search" id="building" value="building" @click="${updateSearchType(controller)}">
                    <label for="building" style="white-space:nowrap">By Building Name</label>
                </input><br>
                <input type="radio" name="search" id="address" value="address" @click="${updateSearchType(controller)}">
                    <label for="address" style="white-space:nowrap">By Address</label>
                </input><br>
                <input type="radio" name="search" id="campus" value="campus" @click="${updateSearchType(controller)}">
                    <label for="campus" style="white-space:nowrap">By Campus</label>
                </input>
            </div>

        </div>
        <div class="equipment-list-container flex">
            ${controller.state.buildings.map((building) => {
  let opened = false;
  if (typeof building._opened !== "undefined") {
    opened = building._opened;
  }
  return html`
              <div class="equipment-list-building row" @click="${toggleEquipment(controller, building)}">
                  <div class="row align-items-center">
                      <div class="select-button" role="button" aria-label="Select Equipment">${opened ?
    html`<img src="${document.baseURI}wwwroot/images/icon_minus.png" srcset="${document.baseURI}wwwroot/images/icon_minus_2x.png 2x" alt="collapse building"/>` : html`<img src="${document.baseURI}wwwroot/images/icon_plus.png" srcset="${document.baseURI}wwwroot/images/icon_plus_2x.png 2x" alt="expand building"/>`}</div> 
                  </div>
                  <div class="row flex equipment-building-text align-items-center">
                    <div>${building.Description} - ${building.Street}, ${building.City}</div> 
                  </div>
              </div>
              ${controller.hasEquipment(building) && opened ? controller.getEquipment(building.Number).map(equipment => html`
                <div class="equipment-list-item row" @click="${selectEquipment(controller, building, equipment)}">
                    <div class="row flex equipment-building-list-item-text">
                      <div>${equipment.Number} - ${equipment.Description} </div> 
                    </div>
                </div>
              </div>`) : html`<div></div>`}
            `
})}
        </div>
    </div> 
</div>
`;

const shipToSelectModal = (controller) => html`
<div class="modal-container" @click="${hideModal(controller)}">
    <div class="modal col">
        <div class="row justify-content-center">
            <h2>Select Ship-To</h2> 
            <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        </div>
        <div class="equipment-list-container mt-32px">
            ${controller.state.details.ShipTos.filter(shipTo => {
  let searchValue = controller.state.shipToSearch;
  if (isNullOrEmpty(searchValue))
    return true;
  return getValue(shipTo.Number, "").indexOf(searchValue) !== -1 || getValue(shipTo.Name, "").indexOf(searchValue) !== -1
    || getValue(shipTo.StreetAddress, "").indexOf(searchValue) !== -1
    || getValue(shipTo.City, "").indexOf(searchValue) !== -1
}).map((shipTo) => {
  return html`
              <div class="equipment-list-building row" @click="${selectShipTo(controller, shipTo)}">
                  <div class="col flex">
                    <div>${shipTo.Number}</div> 
                    <div>${shipTo.Name}</div> 
                    <div>${shipTo.StreetAddress}, ${shipTo.City}</div> 
                  </div>
              </div>`
})}
        </div>
    </div> 
</div>
`;

const shippingTypeModal = (controller) => html`
<div class="modal-container" @click="${hideModal(controller)}">
    <div class="modal col shipping-modal">
        <div class="row justify-content-center">
            <h2>Select Delivery Priority</h2> 
            <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        </div>
        <div class="equipment-list-container mt-32px">
            ${controller.state.details.ShippingTypes.map((shippingType) => {
    return html`
              <div class="equipment-list-building row" @click="${selectShippingType(controller, shippingType)}">
                  <div class="col flex">
                    <div>${shippingType.Name}</div> 
                  </div>
              </div>`;
})}
        </div>
    </div> 
</div>
`;

const chargeModal = (controller) => html`
<div class="modal-container" @click="${hideModal(controller)}">
    <div class="modal col shipping-modal" style="width: 800px">
        <div class="row justify-content-center">
            <h2>Select Charge</h2> 
            <div class="close-modal-icon-container" @click="${hideModal(controller, true)}"><span class="close-modal-icon"></span></div>
        </div>
        <div class="equipment-list-container mt-32px">
              <div class="equipment-list-building row">
                  <div class="col width-110px">
                    <div>Order#</div> 
                  </div>
                  <div class="col flex-1">
                    <div>Work Description</div> 
                  </div>
                  <div class="col width-60px">
                    <div>Est Hours</div> 
                  </div>
                  <div class="col width-60px">
                    <div>Price</div> 
                  </div>
              </div>
            ${!controller.state.charges ? "": controller.state.charges.map((charge) => {
    return html`
              <div class="equipment-list-building row" @click="${selectCharge(controller, charge)}">
                  <div class="col width-110px">
                    <div>${charge.EI_VISIT}</div> 
                  </div>
                  <div class="col flex-1">
                    <div>${charge.WORKDESC}</div> 
                  </div>
                  <div class="col width-60px">
                    <div>${charge.EST_HRSOLD}</div> 
                  </div>
                  <div class="col width-60px">
                    <div>$${charge.PRICE}</div> 
                  </div>
              </div>`;
            })}
        </div>
    </div> 
</div>
`;

function lineTotal(i) {
  return i.Quantity * i.Product.UnitPrice;
}

function invoiceTotal(items) {
  return items.reduce((current, item) => {
    return lineTotal(item) + current
  }, 0);
}

const orderItemList = (controller) => {
  return html`
<div class="container">
    <div class="order-details card row">
        <div class="col d-flex flex-column flex-2 mr-40px">
            <div class="row header-details mb-2">
                <div class="flex col no-gutter justify-content-center">
                    <span class="lighter-text text-small">Order Total</span>
                </div>
                <div class="flex col no-gutter justify-content-end align-items-end">
                    <span class="text-primary invoice-total">$${formatDollar(invoiceTotal(controller.state.cart.items))}</span>
                </div>
            </div>
            <div class="divider"></div>
            <div class="row header-details justify-content-space-between">
                <div class="col no-gutter justify-content-center header-details-label width-60px">
                    <span class="lighter-text text-small">Ship To</span>
                </div>
                <div class="col no-gutter shipto-dropdown">
                      <div class="fake-select-container">
                            ${shipToDropdown(controller)}
                      </div>
                </div>
            </div>
            <div class="row header-details justify-content-space-between">
                <div class="col no-gutter justify-content-center header-details-label">
                    <span class="lighter-text text-small">Delivery Priority</span>
                </div>
                <div class="flex">
                      <div class="fake-select-container">
                            ${shipTypesDropdown(controller)}
                      </div>
                </div>
            </div>
            <div class="row header-details">
                <div class="col no-gutter justify-content-center header-details-label">
                    <span class="lighter-text text-small">Notify</span>
                </div>
                <div class="flex">
                    <div>
                        <input type="email" value="${controller.state.cart.notify}" @change="${onNotifyEmailChange(controller)}"/>
                    </div>
                </div>
            </div>

            <div class="row header-details">
                <div class="col no-gutter justify-content-center header-details-label">
                    <span class="lighter-text text-small">Attention</span>
                </div>
                <div class="flex">
                    <div>
                        <input type="text" maxlength="20" value="${controller.state.cart.attention}" @change="${onAttentionChange(controller)}"/>
                    </div>
                </div>
            </div>

            <div class="row header-details justify-content-space-between">
                <div class="col no-gutter justify-content-center header-details-label">
                    <span class="lighter-text text-small">Charge #</span>
                </div>
                <div class="flex">
                      <div class="fake-select-container">
                            ${chargeDropdown(controller)}
                      </div>
                </div>
            </div>

        </div>
        <div class="col flex-1 ml-40px">
            <div class="row justify-content-space-between align-items-center equipment-container">
                <div class="lighter-text col no-gutter-left text-small">Equipment</div>
                <div @click="${openModal(controller)}"><img src="${document.baseURI}wwwroot/images/icon_magnify.png" srcset="${document.baseURI}wwwroot/images/icon_magnify_2x.png 2x"/></div>
            </div>
            <div class="row mt-18px">
                ${(controller.state.cart.equipment == null || controller.state.cart.building == null) ? html`
                  <div></div>
                ` : html`
                <div class="flex">
                    <div>${controller.state.cart.building.Number}</div>
                    <div>${controller.state.cart.building.Description}</div>
                    <div>${controller.state.cart.building.Street}, ${controller.state.cart.building.City}</div>
                    <div>${controller.state.cart.equipment.Description} - ${controller.state.cart.equipment.Number}</div>
                </div>`}
            </div>
        </div>
    </div>
    ${controller.state.cart.items.map((i, index) => html`
        <div class="card mt-4 row justify-content-space-between cart-items">
            <div class="col justify-content-center width-110px">
                <span class="light-text">${i.Product.Number? i.Product.Number.replace(/^0+/, ''): ''}</span>
            </div>
            <div class="col justify-content-center flex-1">
                ${i.Product.Name}
            </div>
            <div class="col justify-content-center align-items-end width-90px">
                <span class="">$${formatDollar(i.Product.UnitPrice)}</span>
            </div>
            <div class="col justify-content-center width-60px">
                <input type="number text-align-center" min="1" value=${i.Quantity} @change="${updateQuantity(controller, i)}"/>
            </div>
            <div class="col justify-content-center align-items-end width-90px">
                <span class="">$${formatDollar(lineTotal(i))}</span>
            </div>
            <div class="col justify-content-center ml-0">
                <div role="button" @click="${removeItem(controller, index)}"/>
                    <img class="mt-4px" 
                        src="${document.baseURI}wwwroot/images/icon_remove.png" 
                        srcset="${document.baseURI}wwwroot/images/icon_remove_2x.png 2x" 
                        alt="remove item from cart"/> 
                </div>
            </div>
        </div>
    `)}
</div>
${controller.state.showEquipmentModal ? equipmentSelect(controller) : html`<div></div>`}
${controller.state.showSubmitOrderModal ? submitOrderModal(controller) : html`<div></div>`}
${controller.state.showOrderErrorsModal ? orderErrorsModal(controller) : html`<div></div>`}
${controller.state.showOrderWarningsModal ? orderWarningsModal(controller) : html`<div></div>`}
${controller.state.showShipToSelectModal ? shipToSelectModal(controller) : html`<div></div>`}
${controller.state.showShippingTypeModal ? shippingTypeModal(controller) : html`<div></div>`}
${controller.state.showChargeModal ? chargeModal(controller) : html`<div></div>`}
${controller.state.loadingModalMessage.length > 0 ? loadingModal(controller) : html`<div></div>`}
`;
};

class ApplicationController {
    

  constructor() {
    this.user = "";
    this.password = "";

    this.page = "login";

    this.state = {
      cart: {
        shipTo: null,
        shippingType: "",
        equipment: null,
        building: null,
        notify: "",
        items: [],
      },
      canOrder: true,
      catalogUrl: window.catalogUrl,
      details: {},
      equipment: {},// Map<string, <Equipment>[]> - Building number to list of equipment.
      buildings: [],
      total: 0,
      showEquipmentModal: false,
      showShippingTypeModal: false,
      showSubmitOrderModal: false,
      showShipToSelectModal: false,
      loadingModalMessage: "Loading...",
      buildingSearchString: "",
      buildingSearchType: "number",
      shipToSearch: "",
      orderHistory: [],
      orderDetail: null,
      orderWarningsMessages: [],
      showOrderWarningsModal: false,
    };
  }

  beginLoading(message) {
    setBodyNoScroll();
    this.state.loadingModalMessage = message
    this.render();
  }

  endLoading() {
    setBodyScroll();
    this.state.loadingModalMessage = "";
    this.render();
  }

  login(user, password) {
    this.user = user;
    this.password = password;
  }


  getCheckoutDetails(cart) {
    this.beginLoading("Loading...");
    return fetch(document.baseURI + "api/checkout-details", {
      method: "PUT",
      body: JSON.stringify(cart), // body data type must match "Content-Type" header
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic " + btoa(this.user + ":" + this.password),
      }
    }).then(async response => {
      this.state.details = await response.json();
      this.state.cart.items = this.state.details.OrderItems.map(a => ({...a}));
      if (isNullOrEmpty(this.state.cart.notify)) {
        this.state.cart.notify = getValue(this.state.details.UserDetail.EmailAddress, "");
      }
        this.state.canOrder = this.state.details.UserDetail.CanOrder;

        if (!this.state.canOrder) {
            this.showOrderWarning("Your Cart is View Only. Only Field Superintendents can order parts.");
        }


    }).catch(e => alert(e)).finally(() => {
      this.endLoading();
    });
  }

  showOrderWarning(message) {
        let warnings = [];
        warnings.push(message);
        this.state.orderWarningsMessages = warnings;
        this.state.showOrderWarningsModal = true;
    }

  searchBuildings() {
    //User must enter 2 or more chars to search.
    if (!isNullOrEmpty(this.state.buildingSearchString) && this.state.buildingSearchString.length > 1) {
      this.beginLoading("Searching...");
      let url = document.baseURI + 'api/building-search?';
      url += `name=${this.state.buildingSearchString}`;
      url += `&searchType=${this.state.searchType}`;
      return fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Basic " + btoa(this.user + ":" + this.password),
        }
      }).then(async response => {
        this.state.buildings = await response.json();
        this.render();
      }).finally(() => {
        this.endLoading();
      });
    }
  }

  loadCharge(equipment) {
      if (equipment) {
          this.beginLoading("Loading...");
          let url = document.baseURI + 'api/charge?';
          url += `equipment=${equipment.Number}`;
          url += `&userId=${this.user}`;

          return fetch(url, {
              method: "GET",
              headers: {
                  "Content-Type": "application/json",
                  "Authorization": "Basic " + btoa(this.user + ":" + this.password),
              }
          }).then(async response => {
              this.state.charges = await response.json();
              this.render();
          }).catch(e => alert(e)).finally(() => {
              this.endLoading();
          });
      }
  }

  hasEquipment(building) {
    return typeof this.state.equipment[building.Number] !== "undefined";
  }

  getEquipment(number) {
    return this.state.equipment[number];
  }

  loadEquipmentForBuilding(number) {
    this.beginLoading("Loading...");
    let url = document.baseURI + `api/building/${number}/equipment`;

    return fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic " + btoa(this.user + ":" + this.password),
      }
    }).then(async response => {
      this.state.equipment[number] = await response.json();
      this.render();
    }).catch(e => alert(e)).finally(() => {
      this.endLoading();
    });
  }

  orderErrors() {
    let errors = [];
    if (!this.state.canOrder) {
      errors.push("Only Field Superintendents can order parts on SMT.");
      return errors;
    }
    if (isNullOrEmpty(this.state.cart.equipment)) {
      errors.push("Equipment is required.");
    }
    if (isNullOrEmpty(this.state.cart.shipTo)) {
      errors.push("Ship to is required.");
    }
    if (isNullOrEmpty(this.state.cart.shippingType)) {
      errors.push("Delivery Priority is is required.");
    } else {
      if (this.state.cart.shippingType.EmailRequired && isNullOrEmpty(this.state.cart.notify)) {
        errors.push("Notify email is required.");
      }
    }
    if (!isNullOrEmpty(this.state.cart.notify) && !validateEmail(this.state.cart.notify)) {
      errors.push("A valid email address is required.");
    }

    return errors;
  }

  buildOrder() {
    var order = {
      ShippingTypeNumber: this.state.cart.shippingType.Number,
      ShipToNumber:       this.state.cart.shipTo.Number,
      EquipmentNumber:    this.state.cart.equipment.Number,
      NotifyEmail:        this.state.cart.notify,
      LineItems:          this.state.cart.items.map(l => ({
        MaterialNumber: l.Product.Number,
        Quantity:       l.Quantity,
      })),

      Attention:    this.state.cart.attention,
      ChargeNumber: this.state.cart.charge ? this.state.cart.charge.EI_VISIT: "",
    }
    return order;
  }

  submitOrder() {
    let errors = this.orderErrors();
    if (errors.length === 0) {
      let url = document.baseURI + 'api/submit-order';
      this.beginLoading("Submitting Order...");
      return fetch(url, {
        method: "POST",
        body: JSON.stringify(this.buildOrder()), // body data type must match "Content-Type" header
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Basic " + btoa(this.user + ":" + this.password),
        }
      }).then(async response => {
        if (response.status !== 200) {
          let json = await response.json();
          alert(json.error);
        } else {
          this.state.newOrder = await response.json();
          this.state.showSubmitOrderModal = true;
          this.updateCartState(true);
          this.render();
        }
      }).catch(e => alert(JSON.stringify(e.body))).finally(() => {
        this.endLoading();
      });
    } else {
      this.state.showOrderErrorsModal = true;
      this.render();
      setBodyScroll();
    }
  }

  goToOrder(orderNumber) {
    return fetch(document.baseURI + `api/order-detail?number=${orderNumber}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic " + btoa(this.user + ":" + this.password),
      }
    }).then(async response => {
      this.state.orderDetail = await response.json();
      this.page = "order-detail";
      this.render();
    });
  }

  getOrderHistory() {
    let from = moment().subtract(30, 'days').format('YYYY-MM-DD');
    let to = moment().format('YYYY-MM-DD');
    return fetch(document.baseURI + `api/order-history?from=${from}&to=${to}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic " + btoa(this.user + ":" + this.password),
      }
    }).then(async response => {
      this.state.orderHistory = await response.json();
      this.render();
    });
  }

  history() {
    this.page = "order-history";
    this.getOrderHistory()
  }

  init() {
    window._cnbs_sendAppMessage = this.handleActionPayload.bind(this);
    if (typeof invokeCSharpAction === "undefined") {
      this.page = "login";
      this.render();
    } else {
      invokeCSharpAction({
        "action": "checkout-ready",
        "payload": null
      });
    }
  }

  updateCartState(forceClear = false) {
    let shouldClear = this.state.cart.items.length === 0 || forceClear;
    let cartObject = {
      header: {
        shipTo: shouldClear ? null : this.state.cart.shipTo,
        shippingType: shouldClear ? "" : this.state.cart.shippingType,
        equipment: shouldClear ? null : this.state.cart.equipment,
        building: shouldClear ? null : this.state.cart.building,
        notify: shouldClear ? "" : this.state.cart.notify,
        canOrder: this.state.canOrder,
        catalogUrl: this.state.catalogUrl,
      },
      lineItems: forceClear ? {} : this.state.cart.items.reduce((o, item) => {
        o[item.Product.Number] = item.Quantity;
        return o;
      }, {})
    };
    if (typeof invokeCSharpAction === "undefined") {
      console.log(cartObject);
    } else {
      invokeCSharpAction({
        action: "set-cart-state",
        payload: cartObject,
      });
    }
  }

  render() {
    console.log(this.state);
    if (this.page === "checkout") {
      _render([orderHeader(this), orderItemList(this)], document.body);

      var isChecked = false;
      var rb1 = document.getElementById("building"); 
      var rb2 = document.getElementById("address"); 
      var rb3 = document.getElementById("campus"); 
      if (rb1 && rb2 && rb3) {
          isChecked = rb1.checked || rb2.checked || rb3.checked;
      }

      if (!isChecked) {
          var rb = document.getElementById(window.defaultSearchType);
          if ( rb ) {
              rb.checked = true;
          } else {
              rb = document.getElementById("building");
              if ( rb ) rb.checked = true;
          }
      }
    }
    if (this.page === "order-history") {
      _render(orderHistory(this), document.body);
    }
    if (this.page === "order-detail") {
      _render(orderDetails(this), document.body);
    }
    if (this.page === "login") {
      _render(manualLogin(this), document.body);
    }
  }

  handleActionPayload(actionPayload) {
    if (actionPayload.action === "checkout") {
      this.handleCheckoutAction(actionPayload.payload);
    }
  }

  handleCheckoutAction(payload) {
    this.page = "checkout";

    // Call login to set login values.
    this.login(payload.username, payload.password);
    // Map payload cart details to cart object in controller
    this.state.cart.shippingType = payload.cart.header.shippingType;
    this.state.cart.shipTo = payload.cart.header.shipTo;
    this.state.cart.equipment = getValue(payload.cart.header.equipment, null);
    this.state.cart.building = getValue(payload.cart.header.building, null);
    this.state.cart.notify = payload.cart.header.notify;
    this.state.catalogUrl = payload.cart.header.catalogUrl;

    // Get product details and dropdown menu options.
    this.getCheckoutDetails(payload.cart.lineItems).then(() => {
      this.render();
    });
  }
}

export const app = new ApplicationController();

export function getApp() {
  return app;
}


//
//
