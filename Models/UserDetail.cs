namespace smt_dashboard.Models
{
    public class UserDetail
    {
        public bool CanOrder { get; set; } 
        public string EmailAddress { get; set; } 
    }
}