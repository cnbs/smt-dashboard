using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard.Models
{
    // public class GetChargeRequestDTO
    // {
    //     public string EquipmentNumber { get ; set ; }
    //     public string UserId { get ; set ; }
    // }
    //
    public class GetChargeRequestDTO
    {
        [JsonProperty("para")]
        public List<GetChargeRequestDTO_Para> Para { get; set; }
    }

    public class GetChargeRequestDTO_Para
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
