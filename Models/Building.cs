using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace smt_dashboard.Models
{
    public class Building
    {
        public string Number { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
    }
}