using System.Collections;
using System.Collections.Generic;

namespace smt_dashboard.Models
{
    public class NewOrder
    {
        public string ShippingTypeNumber { get; set; }
        public string ShipToNumber { get; set; }
        public string EquipmentNumber { get; set; }
        public string NotifyEmail { get; set; }
        
        public IEnumerable<NewOrderItems> LineItems { get; set; }

        public string Attention { get; set; }
        public string ChargeNumber { get; set; }
    }

    public class NewOrderItems
    {
       public string MaterialNumber { get; set; } 
       public int Quantity { get; set; } 
    }
}