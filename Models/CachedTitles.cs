using smt_dashboard.Interfaces;

namespace smt_dashboard.Models
{
    public class CachedTitles : ICachedTitles
    {
        public string NonConformancesTitle { get; set; } = "Non Conformances";
        public string VitalFewObjectivesYTDTitle { get; set; }
        public string OutOfServiceUnitsTitle { get; set; } = "Out of service Units";
        public string MTDMaterialHPDTitle { get; set; }
        public string SchindlerAheadTitle { get; set; } = "Schindler Ahead";
        public string SickUnitsTitle { get; set; }
        public string LingeringOpenCallsTitle { get; set; }
    }
}
