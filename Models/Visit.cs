using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard.Models
{
    public class Visit
    {
        [JsonProperty("ORDERS")]
        public List<Visit_ORDER> ORDERS { get; set; }
    }

    public class Visit_ORDER
    {
        [JsonProperty("EI_VISIT")]
        public string VISIT { get; set; }

        [JsonProperty("WORKDESC")]
        public string WORKDESC { get; set; }

        [JsonProperty("EST_HRSOLD")]
        public decimal ESTHRSOLD { get; set; }

        [JsonProperty("PRICE")]
        public decimal PRICE { get; set; }
    }
}
