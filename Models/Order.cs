using System.Collections;
using System.Collections.Generic;

namespace smt_dashboard.Models
{
    public class Order
    {
        public string Number { get; set; }
        public string Name { get; set; }
        
        public Equipment Equipment { get; set; }
        
        public ShippingType ShippingType { get; set; }
        
        public ShipTo ShipTo { get; set; }

        public IEnumerable<OrderItem> Items { get; set; }= new List<OrderItem>();
    }

    public class OrderItem
    {
        public ProductDetail Product { get; set; }
        public decimal Quantity { get; set; }
    }
}