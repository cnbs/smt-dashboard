﻿namespace smt_dashboard.Models
{
    public class RepairType
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string RepairClass { get; set; }
    }
}
