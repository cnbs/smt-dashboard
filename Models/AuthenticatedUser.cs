using System.Text;
using Microsoft.AspNetCore.WebUtilities;

namespace smt_dashboard.Models
{
    public class AuthenticatedUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string SupervisorIdListString { get; set; }
        public string AuthorizationHeader => $"Basic {Base64Encode($"{Username}:{Password}")}";
        Encoding encoding = Encoding.UTF8;
        private string Base64Encode(string raw)
        {
            string base64 = Base64UrlTextEncoder.Encode(encoding.GetBytes(raw));
            return base64.PadRight(base64.Length + (4 - base64.Length % 4) % 4, '=');
        }
    }
}
