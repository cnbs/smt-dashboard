﻿using Newtonsoft.Json;

namespace smt_dashboard.Models
{
    public class UpdateOrderDTO
    {
        [JsonProperty("FMORDER")]
        public UpdateOrderDTO_FMORDER FMORDER { get; set; }
    }

    public class UpdateOrderDTO_FMORDER
    {
        [JsonProperty("I_ORDER_Number")]
        public string OrderNumber { get; set; }

        [JsonProperty("i_date")]
        public string Date { get; set; }

        [JsonProperty("i_tech1")]
        public string Tech1 { get; set; }

        [JsonProperty("i_tech2")]
        public string Tech2 { get; set; }

        [JsonProperty("i_field_instr")]
        public string FieldInstr { get; set; }

        [JsonProperty("i_est_labor_hrs")]
        public decimal EstLaborHrs { get; set; }

        [JsonProperty("i_est_matl_cost")]
        public decimal EstMatlCost { get; set; }
    }

}
