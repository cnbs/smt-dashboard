﻿namespace smt_dashboard.Models
{
    public class ResponseStatus
    {
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }

        public bool IsSuccess { get; set; }
    }
}