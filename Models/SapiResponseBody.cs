using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace smt_dashboard.Models
{
    public class SapiResponseBody<T>
    {
        [JsonProperty("DATA")]
        public T Data { get; set; }
        
        [JsonProperty("STATUS")]
        public SapiStatus Status { get; set; }
    }

    public class SapiStatus
    {
        [JsonProperty("MSGTY")]
        public string Status { get; set; }
        [JsonProperty("MSGID")]
        public string MessageId { get; set; }
        [JsonProperty("MSGNO")]
        public string MessageNumber { get; set; }
        [JsonProperty("MSGLN")]
        public string MessageLanguage { get; set; }
        [JsonProperty("JSON")]
        public dynamic Json { get; set; }
    }
}
