﻿using System.Collections;
using System.Collections.Generic;

namespace smt_dashboard.Models
{
    public class FmOrder
    {
        public string EquipmentNumber { get; set; }

        public string RepairCode { get; set; }

        public string FieldInstructions { get; set; }

        public string EstLaborHours { get; set; }

        public string EstMaterialCost { get; set; }

        public string Currency { get; set; }

        public string PersonnelNumber { get; set; }

        public string StartDate { get; set; } //YYYYMMDD

        public string Description { get ; set ; }

        public string PersonnelNumber2 { get; set; }
        public string OrderNumber { get ; set ; }
        public string PersonnelName { get ; set ; }
        public string PersonnelName2 { get ; set ; }
        public string RepairName { get ; set ; }
    }
}
