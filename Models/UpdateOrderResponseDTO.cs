﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace smt_dashboard.Models
{
    public class UpdateOrderResponseDTO
    {
        [JsonProperty("DATA")]
        public UpdateOrderResponseDTO_DATA DATA { get; set; }

        [JsonProperty("STATUS")]
        public STATUS STATUS { get; set; }
    }
 
    public class UpdateOrderResponseDTO_DATA
    {
        [JsonProperty("GEN")]
        public List<object> GEN { get; set; } = new List<object>(0);
    }

}
