using System.Collections;
using System.Collections.Generic;

namespace smt_dashboard.Models
{
    public class CheckoutDetails
    {
        public IEnumerable<ShipTo> ShipTos { get; set; } 
        public IEnumerable<ShippingType> ShippingTypes { get; set; } 
        public IEnumerable<OrderItem> OrderItems { get; set; } 
        
        public UserDetail UserDetail { get; set; }
    }
}