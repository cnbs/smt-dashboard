using System;

namespace smt_dashboard.Models
{
    public class OrderHistory
    {
        public string Number { get; set; }
        public string Description { get; set; }
        public double Total { get; set; }
        public DateTime Date { get; set; }
    }
}