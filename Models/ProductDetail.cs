namespace smt_dashboard.Models
{
    public class ProductDetail
    {
        public double UnitPrice { set; get; }
        public string Name { set; get; }
        public string Number { set; get; }
        
        public string Currency { set; get; }
    }
}