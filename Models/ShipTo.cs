namespace smt_dashboard.Models
{
    public class ShipTo
    {
        public string Number { get; set; }
        public string Name { get; set; }
        
        public string City { get; set; }
        
        public string StreetAddress { get; set; }
    }
}