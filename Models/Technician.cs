﻿namespace smt_dashboard.Models
{
    public class Technician
    {
        public string PersonnelNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string TechFullName => $"{FirstName} {LastName}";
    }
}
