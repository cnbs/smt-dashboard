using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace smt_dashboard.Models
{
    public class Equipment
    {
        public string Number { get; set; }

        public string Description { get; set; }

        public string PersonnelNumber { get; set; }

        public string OfficeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Attention { get; set; }
    }
}