namespace smt_dashboard.Models
{
    public class ShippingType
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public bool EmailRequired { get; set; }
    }
}