### Deploying SMT Webapp

To build the application for deployment run the below command. (Delete the old Build directory first if it is there)

`dotnet publish -o Build`

Add the contents of the new `Build` directory to a zip file. If you pay attention you only need to grab the files that have been recently modified and the wwwroot directory.

Connect to Schindler's VPN through either `myaccess.schindler.com`
or through a Schindler laptop.

Use remote desktop to connect to the server:

- QA server: `secwsr1510.global.schindler.com`
- PROD server: `secwsr1511.global.schindler.com`

Open IIS - Internet Information Services
Go to the list of sites. Find the SMT site and click "Explore" this will open the directory that the application is at.

Copy the contents of the zip to this directory. You may have to temporarily stop the server to copy the files.

#####URLS
QA:
https://eisupweb-qa.schindler.com/ - requires CNBS or Schindler VPN

PROD
https://eisupweb.schindler.com/

Running the Dashboard through a browser:  
By adding the showlogin=true parameter to the URL, you can login to the dashboard from a browser instead of from within the mobile app.

https://eisupweb.schindler.com/v3?showlogin=true