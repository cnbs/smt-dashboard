using System;

namespace smt_dashboard.ViewModels
{
    public class DashboardViewModel
    {
        public bool ShowLoginPage { get; set; }
        public string SAPServer { get; set; }
        public string AppId { get; set; }
        public string BuildNumber { get; set; }
        public string CatalogUrl { get; set; }
        public string FMOrderUrl { get; set; }
        
        public string Base { get; set; }
    }
}
